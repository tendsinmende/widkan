# Widkan

A widget toolkit based on Vulkan (via Marp-vk) and Rust. 

# Disclaimer
This was my first attempt on a vulkan based ui-toolkit. While it had many features, some aspects of it where flawed. I took what I learned and started to create a second version: [neith](https://gitlab.com/tendsinmende/neith).
At the time of writing this, neith is not yet fully featured, but has already a much better design. This library is **NOT** maintained anymore and has several bugs I'm too lazy to fix.

# Library Design
As far as I undertood the design I favor is called "retained". The interface will mostly be designed before starting. Widgets can be changed afterwards via interrior mutability.
The whole interface can be understood as an Tree made from widgets. 

### Events 
When the application recives a Event, it will be passed down the tree. Some widgets can filter out events if the child widgets should not recive those. For instance clicks outside of
the virtual area of an viewport.

### Wigets
The widgets can be split into three types
- helper
- Layouting
- Input/Output

The layout widgets only defines layour related taskes. Like a "ListOfElements" or "HorizontalSplit". The Presenting one are more interesting since they are actually drawn. They obey the layouting given to them by their parent node and draw primitives.
This can be for instance a "TextBox" which consists of a Frame, the backround and many "characters" making up the text.

### Primitives
The primitives are the atomic parts of the interface. Everything is made from those. I currently plan to implement the following primitivs:

- Text
- Box
- Line
- image
- Icon

### Windowing and rendering
The library will be able to create the Vulkan context by it self and provide the window and event handling based on `Winit` and `Marp`. You'll then only have to call something like 

```
interface.update();
```
to update the whole interface.

However, you can also choose to provide the `Winit` events and a `Device` from `Marp-Vk` to render the final UI Image.
In that case you provide the events and the extent you want to render to and will be given a `Arc<Image>` as well as an `Arc<Fence>` or `Arc<Semaphore>` which signal when rendering of the image has finished. You can then copy the image in you own vulkan application to the correct location or add for 
instance your own renderers final image to it.

## Building

### Dependencies
- xorg-dev (for clipboard handing, see https://github.com/aweinstock314/rust-clipboard)
- glslangValidator (for shader compilation via the `compile_shader.sh` script)

### Shader recompiling
Currently the library is relying on the git version of `Marp` since both librarys are developing rather fast. 
To compile the library shaders execute `resources/shader/compile_shader.sh` via

```
cd widkan/resources/shader
chmod +x ./compile_shader.sh
./compile_shader.sh
```

### Build the Library
To build the library do the following in the root directory

```
cargo build --release
```

To test some of the examples remain in the root directory and do

```
cargo run --example simple --release
```

*Note on the `--release` flag*
Since the `image` crate is really slow in non release mode, but used for icons in this library, you really should use this flag.

## Usage in other librarys
You can include the crate itself via the normal

``` 
[dependencies]
widkan = {git = "https://gitlab.com/Siebencorgie/widkan", branch = "master"}
```
statement in the `Cargo.toml`.

However, to load the needed resources, you'll also have to place the resource folder `widkan/resources/` somewhere and point the config file to it. Otherwise the crate won't find the shaders and other resources like fonts and the icons. 

**If you don't want to change anything, just download the `widkan` sub-folder of this crate, place it into your projects root and delete everything within the `widkan` subfolder apart from the `resources` directory, that should work.**

A better method would be to add a download statement into a build.rs file which download the needed resources.
