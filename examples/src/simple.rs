/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use widkan;
use widkan::widget::{io::*, helper::*, layout::*, *};
use widkan::widget::events::*;
use widkan_window;
use widkan_window::*;

use std::sync::{Arc, RwLock};

fn main() {
    let mut working = Arc::new(RwLock::new(true));

    //Create a widkan-window
    let mut window = Window::new(
        None,
        "default_config.txt"
    ).expect("Failed to create widkan-window window");

    let config = &window.get_config();

    //This will be our list for the list view

    let first_labels = vec!["First item", "Second item", "Third item", "Even more items"];
    let edits = vec!["Here we have an edit yes!", "More edits to come", "More test edits in this list yes.."];
    let second_labels = vec!["And now more labels", "Oh lawd", "more labels coming!"];
    
    let list = list::List::new();
    for l in first_labels{
        list.push(Label::new(
            config, l, None, None
        ));
    }

    for w in edits{
        list.push(TextEdit::new(
            config, w, None, Some(Alignment::Center)
        ));
    }

    list.push(ColorText::new(
        "Colored Text".to_owned(),
        16,
        [1.0, 0.2, 0.8, 1.0]
    ));

    list.push(
        button::Button::<()>::new(
            config,
            //io::ButtonContent::Icon(Ico::Arrow),
            io::ButtonContent::IconTextVertical((Ico::Home, "Click Me!".to_string())),
            io::Action::static_action(||{println!("Hello from the button click with a label")}),
        )
    );
    list.push(
        button::Button::<()>::new(
            config,
            io::ButtonContent::Icon(Ico::Save),
            io::Action::static_action(||{println!("Hello from the button click without a label")}),
        )
    );
    
    for l in second_labels{
        list.push(Label::new(
            config, l, None, None
        ));
    }

    let bar_items: Vec<(String, Arc<dyn Widget + Send + Sync>)> = (0..5).into_iter().map(|i|{
        let name = String::from("Sub: ") + &i.to_string();
        let widget: Arc<dyn Widget + Send + Sync> = io::Label::new(
            &config,
            &(String::from("Widget Number: ") + &i.to_string()),
            None,
            None
        );

        (name, widget)
    }).collect();

    let bar = layout::Bar::new(&config, bar_items);

    let cancel_flag = working.clone();
    let f = frame::Frame::new(
        config,
        Some(
            layout_box::LayoutBox::new(
               config,
                Some(
                    layout_box::LayoutBox::new(
                        config,
                        Some(
                            layout_box::LayoutBox::new(
                                config,
                                Some(bar),
                                Some(
                                    diagram::Diagram::new(
                                        |x| x.sin() + (2.0*(1.3*x+0.5).cos()*2.0),
                                        [1.0,0.0,0.0],
                                        4.0,
                                        (-8.0, -16.0),
                                        (8.0, 16.0)
                                    )
                                ),
                                layout_box::SplitType::HorizontalMoveable(layout::Offset::PercentLeft(0.5)),
                            )
                        ),
                        Some(
                            ListView::new(
                                config, list
                            )
                        ),
                        layout_box::SplitType::VerticalFixed(layout::Offset::PixelLeft(150.0)),
                    )
                ),
                Some(
                    layout_box::LayoutBox::new(
                        config,
                        Some(
                            text_view::TextView::new(
                                config,
                                "I am some nice Text view, which can be multiline and, if needed changes into a viewport!",
                                Some(50),
                                Some(Alignment::UpLeft)
                            )
                        ),
                        Some(
                            layout_box::LayoutBox::new(
                                config,
                                Some(
                                    button::Button::<()>::new(
                                        config,
                                        //io::ButtonContent::Icon(Ico::Arrow),
                                        io::ButtonContent::IconTextVertical((Ico::File, "Close".to_string())),
                                        io::Action::static_action(move ||{
                                            *cancel_flag.write().unwrap() = false;
                                        }),
                                    )
                                ),
                                Some(
                                    layout::FloatingArea::new(
                                        number_edit::NumberEdit::new(
                                            config,
                                            100.0,
                                            None,
                                            None,
                                        ),
                                        Area{
                                            pos: (0.0,0.0),
                                            extent: (512.0, config.default_single_line_height)
                                        },
                                        Positioning::Alignment(Alignment::Center)
                                    )
                                    
                                ),
                                layout_box::SplitType::HorizontalFixed(layout::Offset::PercentLeft(0.25)),
                            )
                        ),
                        layout_box::SplitType::HorizontalMoveable(layout::Offset::PercentLeft(0.5)),
                    )
                ),
                layout_box::SplitType::VerticalMoveable(layout::Offset::PercentRight(0.25)),
            )
        )
    );

    window.interface().set_root(f);

    window.update();
    
    //window.get_config().save().expect("Failed to save last config...");
}
