/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */


use widkan_window;

#[test]
fn send_interface() {

    let window = widkan_window::Window::new(
        None,
        "default_config.txt"
    ).expect("Could not create testing window");

    let interface = window.interface();
    
    let _thread_handle = std::thread::spawn(
        move||{
            let mut conf = interface.get_config();
            conf.scroll_speed = 1.0;
        }
    );
}
