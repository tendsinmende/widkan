/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */


#version 450
#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout (location = 0) in vec2 uv;
layout (location = 0) out vec4 uFragColor;

layout (set = 0, binding = 0) uniform sampler2D character_atlas;
layout (set = 0, binding = 1) uniform sampler2D icon_atlas;

layout (push_constant) uniform PushConsts{
  mat4 transform;
  vec4 loc_extent;
  float level;
}pc;

void main() {
  //Calculate the uv position for the supplied character_mm
  vec2 width_height = pc.loc_extent.zw - pc.loc_extent.xy;
  vec2 character_uv = vec2(pc.loc_extent.xy + (uv * width_height));
  uFragColor = texture(icon_atlas, character_uv);
}
