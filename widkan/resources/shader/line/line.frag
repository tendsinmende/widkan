/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#version 450

layout (location = 0) in vec4 pos;

layout (location = 0) out vec4 uFragColor;

layout (push_constant) uniform PushConsts{
  mat4 transform;
  vec4 color;
  float level;
  vec3 pad;
}pc;

void main() {  
  uFragColor = pc.color;
}
