/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#version 450

layout (location = 0) in vec2 uv;
layout (location = 1) in vec4 pos;

layout (location = 0) out vec4 uFragColor;

layout (push_constant) uniform PushConsts{
  mat4 transform;
  vec4 color1;
  vec4 color2;
  float level;
  vec3 pad1;
}pc;

float rand(vec2 co){
    return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}

void main() {  
  uFragColor = mix(pc.color1, pc.color2, rand(pos.xy));
}
