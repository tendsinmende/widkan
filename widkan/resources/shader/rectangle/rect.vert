/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#version 450

//Standard vertex layout
layout (location = 0) in vec4 pos;
layout (location = 1) in vec2 uv;


layout (push_constant) uniform PushConsts{
  mat4 transform;
  vec4 color1;
  vec4 color2;
  float level;
  vec3 pad1;
}pc;

layout (location = 0) out vec2 out_uv;
layout (location = 1) out vec4 out_pos;

void main() {
  out_uv = uv;
  vec4 inter_pos = pc.transform * pos; 
  gl_Position = vec4(inter_pos.xy, pc.level, 1.0);
  out_pos = vec4(inter_pos.xy, pc.level, 1.0);
}
