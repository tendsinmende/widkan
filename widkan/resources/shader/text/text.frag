/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */


#version 450
#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout (location = 0) in vec2 uv;
layout (location = 0) out vec4 uFragColor;


layout (set = 0, binding = 0) uniform sampler2D character_atlas;
layout (set = 0, binding = 1) uniform sampler2D icon_atlas;

layout (push_constant) uniform PushConsts{
  mat4 transform;
  vec4 character_mm;
  vec4 color;
  float level;
  vec3 pad1;
}pc;

float rand(vec2 co){
    return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}

void main() {

  vec4 text_color = pc.color;
  
  //Calculate the uv position for the supplied character_mm
  vec2 width_height = pc.character_mm.zw - pc.character_mm.xy;
  vec2 character_uv = vec2(pc.character_mm.xy + (uv * width_height));
  
  vec4 tex_col = texture(character_atlas, character_uv);
  vec4 col = vec4(text_color.rgb, tex_col.a);
  uFragColor = col;
}
