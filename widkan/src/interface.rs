/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

//! # Interface
//! The interface module holds the main entry point to the whole library.
//! It handles resource creation as well as the rending of said resources
//! for you.
//! # Safety when using Vulkan objects
//! The library assumes that you are rendering the interface on the same queue as the queue given as `init_queue`
//! when creating the interface struct.
//!
//! All resources are exclusively owned by this queue and the command buffers etc. are also created for that queue.
//! If you want to have the result image on some other queue, consider to copy it, since a queue-transfer would break the library's handling of its
//! Sub resources.
//!
//! You could also choose to do two queue-transfers, but make sure that they finished before calling `update()` again, since that will be used to write to that image
//! again. Note: Queue-Transfer for the renderer is currently not implemented!

use marp;
use marp::ash::vk;
use marp::command_buffer::*;
use marp::device::*;
use marp::image::*;
use std::sync::{Arc, RwLock};

use crate::config::Config;
use crate::rendering::*;
use crate::widget::events::*;
use crate::widget::*;

use crate::widget::events::Event;

///Main Interface struct. Holds the current widget-tree as well as the renderer and its resources.
pub struct Interface {
    /// The renderer and its resources
    renderer: RwLock<Renderer>,
    /// The Root widget
    widgets: RwLock<Option<Arc<dyn Widget + Send + Sync>>>,
    ///The rendering area for this interface
    area: RwLock<Area>,
    config: RwLock<Config>,
}

impl Interface {
    ///Creates a new interface for a given extent. If the extent changes, call `update_extent` which will take care of resource
    /// recreation.
    pub fn new(
        device: Arc<Device>,
        init_queue: Arc<Queue>,
        extent: (f64, f64),
        config_path: &str,
        final_image_format: vk::Format,
    ) -> Arc<Self> {
        //Pre load config
        let config = Config::load(config_path);

        Arc::new(Interface {
            renderer: RwLock::new(Renderer::new(
                device,
                init_queue,
                extent,
                &config,
                final_image_format,
            )),
            widgets: RwLock::new(None),
            area: RwLock::new(Area {
                pos: (0.0, 0.0),
                extent: extent,
            }),
            config: RwLock::new(config),
        })
    }

    ///Sets the root widget of the interface. The widget can also contain sub widgets. So it is possible to swap out whole trees.
    pub fn set_root(&self, widget: Arc<dyn Widget + Send + Sync>) {
        *self.widgets.write().expect("Could not set root widget") = Some(widget);
    }

    ///Renders a new version of the interface to the target image.
    /// #Safety
    /// Assumes that the command buffer is in recording state, but not within any render_pass.
    pub fn render(
        &self,
        command_buffer: Arc<CommandBuffer>,
    ){
        self.renderer.write().expect("Could not clear render calls").clear_calls();
        
        match *self.widgets.write().expect("Could not get root widget"){
            Some(ref wid) => {
                
                //now render the whole thing
                let rendering_calls = wid.render(
                    &mut *self.renderer.write().expect("Failed to update sub widgets"),
                    0, 
                );
                //Add the rendering calls to the renderer
                self.renderer.write().expect("Could not add rendering calls to renderer!").add_primitives(rendering_calls);
                
            },
            None => println!("Could not render non existent root widget"),
        }

        self.renderer
            .write()
            .expect("Failed to lock renderer for rendering")
            .render(command_buffer);
    }

    /// Updates the interface with `events`.
    /// Returns state information which must be handled by the host. For instance mouse cursor settings.
    pub fn update(
        &self,
        events: &Vec<Event>,
        render_queue: Arc<Queue>
    ) -> UpdateRetState {        
        //Check the events if we got a resize event. if so, resize renderer to the new extent.
        let mut most_recent_area = self.area();
        for ev in events.iter() {
            if let EventType::Resize(new_area) = ev.event {
                most_recent_area = new_area;
            }
        }

        //Update the renderer with the new area.
        if self.area() != most_recent_area{
            self.renderer
                .write()
                .expect("Failed to resize renderer")
                .resize(render_queue.clone(), most_recent_area);    
        }

        let mut update_state: UpdateRetState = Default::default();
        
        //Update widgets with new events and let them add their final rendering commands
        match *self
            .widgets
            .write()
            .expect("Failed to read Interface root widget!")
        {
            Some(ref wid) => {
                //Update the tree with the events we got, but use the "old" area, since that is used
                update_state = wid.update(
                    most_recent_area,
                    events,
                    &*self
                        .renderer
                        .read()
                        .expect("Could not read renderer while updating wiget tree"),
                );
            }
            None => println!("widkan: WARNING: no root widget is set, rendering nothing!"),
        }
        //Update the interface area to the new one.
        *self.area.write().expect("Failed to read interface area") = most_recent_area;

        update_state
    }

    ///Returns the area which is currently used as Window size for all sub resources.
    pub fn area(&self) -> Area {
        *self.area.read().expect("Failed to read area!")
    }

    ///Returns the most recent image.
    /// # Safety
    /// If you are using this image to copy content from it, you'll have to handle synchronization your self.
    /// The renderer has only this image internally. When calling `update()` on this interface, it will overwrite
    /// this image.
    /// The returned image is in TRANSFER_DST_OPTIMAL layout, since it should be used to be copied either to a swapchain or blit to some other image.
    pub fn get_image(&self) -> Arc<Image> {
        self.renderer
            .read()
            .expect("Failed to read current color image.")
            .get_current_color_image()
    }

    pub fn get_config(&self) -> Config {
        self.config.read().expect("Could not read interfaces inner config").clone()
    }

    ///Sets the config within this interface
    pub fn set_config(&self, new_config: Config){
        *self.config.write().expect("Could not override inner interface config!") = new_config;
    }
}
