/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use marp::command_buffer::*;
use marp::descriptor::*;
use marp::device::*;
use marp::framebuffer::RenderPass;
use marp::image::*;
use marp::memory::*;
use marp::pipeline::*;
use marp::sampler::*;
use marp::shader::*;
use marp::buffer::*;

use marp::ash::vk;

use image;

use crate::config::Config;
use crate::rendering::primitive::*;
use crate::rendering::Renderer;
use std::sync::Arc;

use std::u64;

pub const ICONS_PER_LINE: usize = 8;


fn default_icon_vertex_shader() -> Vec<u32>{
    let bytes = include_bytes!("../../resources/shader/icon/vert.spv");
    marp::ash::util::read_spv(&mut std::io::Cursor::new(&bytes[..])).expect("Could not read icon shader!")
}

fn default_icon_frag_shader() -> Vec<u32>{
    let bytes = include_bytes!("../../resources/shader/icon/frag.spv");
    marp::ash::util::read_spv(&mut std::io::Cursor::new(&bytes[..])).expect("Could not read icon shader!")
}

fn default_icon_map() -> &'static [u8]{
    let bytes = include_bytes!("../../resources/images/IconMap.png");
    bytes
}

///Used to choose the Icon that should be drawn. TODO actually come up with a good icon set.
#[derive(Clone, Copy, Debug)]
pub enum Ico {
    Folder,
    Save,
    File,
    Home,
    ArrowRight,
    ArrowDown,
    ArrowLeft,
    ArrowUp
    
}

impl Ico{

    pub fn to_idx(&self) -> usize{
        match self{
            Ico::Folder => 0,
            Ico::Save => 1,
            Ico::File => 2,
            Ico::Home => 3,
            Ico::ArrowRight => 4,
            Ico::ArrowDown => 5,
            Ico::ArrowLeft => 6,
            Ico::ArrowUp => 7,
        }
    }
    
    pub fn get_location_and_extent(&self) -> [f32;4]{
        let uv_size = 1.0 / ICONS_PER_LINE as f32;

        let x_top = (self.to_idx() % ICONS_PER_LINE) as f32 * uv_size;
        let y_top = (self.to_idx() / ICONS_PER_LINE) as f32 * uv_size; 
        [
            x_top,y_top,
            x_top + uv_size, y_top + uv_size
        ]
    }
}

pub struct IconBuilder {
    index_buffer: Arc<Buffer>,
    vertex_buffer: Arc<Buffer>,
    //Holds the image and the sampler for the icons
    image_and_sampler: (Arc<Image>, Arc<Sampler>)
}

impl IconBuilder{
    pub fn new(
        config: &Config,
        device: Arc<Device>,
        init_queue: Arc<Queue>,
    ) -> Self{
        
        let indices: Vec<u32> = vec![0, 1, 2, 2, 3, 0];
        let (index_upload, index_buffer) = Buffer::from_data(
            device.clone(),
            init_queue.clone(),
            indices.clone(), //The buffer size we need
            BufferUsage {
                index_buffer: true,
                storage_buffer: true,
                ..Default::default()
            },
            SharingMode::Exclusive,
            MemoryUsage::GpuOnly //we only want device access for this buffer.
        )
        .expect("Failed to create index buffer");
        index_upload.wait(std::u64::MAX).expect("Failed to wait for index upload!");
        //We go from 0.0 to 1.0, since we then can scale appropriate and afterwards move the rectangle to the right position
        let vertices = vec![
            Vertex {
                pos: [0.0, 1.0, 0.0, 1.0],
                uv: [0.0, 1.0],
            },
            Vertex {
                pos: [1.0, 1.0, 0.0, 1.0],
                uv: [1.0, 1.0],
            },
            Vertex {
                pos: [1.0, 0.0, 0.0, 1.0],
                uv: [1.0, 0.0],
            },
            Vertex {
                pos: [0.0, 0.0, 0.0, 1.0],
                uv: [0.0, 0.0],
            },
        ];

        let (vertex_upload, vertex_buffer) = Buffer::from_data(
            device.clone(),
            init_queue.clone(),
            vertices,
            BufferUsage {
                vertex_buffer: true,
                ..Default::default()
            },
            SharingMode::Exclusive,
            MemoryUsage::GpuOnly, //we only want device access for this buffer.
        )
        .expect("Failed to create index buffer");

        vertex_upload.wait(std::u64::MAX).expect("Failed to wait for vertex upload!");

        
        //Prepare the image that holds all icons
        let upload_image = if &config.image_directory != "default"{
            image::open(config.image_directory.clone() + "IconMap.png").expect(&format!["Could not find icon map at {}", config.image_directory]).to_rgba()
        }else{
            println!("LOAD DEFAULT IMAGE");
            image::load_from_memory(default_icon_map()).expect("Failed to load default icon map!").to_rgba()
        };
        let image_dimensions = upload_image.dimensions();
        let image_data = upload_image.into_raw();

        let (image_upload, image) = Image::from_data(
            device.clone(),
            init_queue.clone(),
            ImageInfo::new(
                ImageType::Image2D {
                    width: image_dimensions.0,
                    height: image_dimensions.1,
                    samples: 1,
                },
                vk::Format::R8G8B8A8_UNORM,
                None,                        //No extra ordinary component mapping
                Some(MipLevel::Specific(1)), //Only one mip map for now
                ImageUsage {
                    sampled: true,
                    //transfer_dst: true,
                    color_attachment: true,
                    ..Default::default()
                },
                MemoryUsage::GpuOnly,
                None, //Optimal tiling by default
            ),
            SharingMode::Exclusive,
            image_data,
        )
            .expect("Failed to create image");
        image_upload
            .wait(u64::MAX)
            .expect("Failed to wait for image upload!");

        //Create a smapler which will be used to sample from the image.
        //TODO optimize for sharpness
        let sampler = SamplerBuilder::default()
            .build(device.clone())
            .expect("Failed to create sampler!");

        IconBuilder{
            vertex_buffer,
            index_buffer,
            image_and_sampler: (image, sampler)
        }
    }

    pub fn get_image_and_sampler(&self) -> (Arc<Image>, Arc<Sampler>){
        self.image_and_sampler.clone()
    }

    pub fn build(
        self,
        config: &Config,
        device: Arc<Device>,
        _init_queue: Arc<Queue>,
        render_pass: Arc<RenderPass>,
        desc_set_layouts: Vec<vk::DescriptorSetLayout>,
    ) -> Icon{
        //Create the pipeline that handles the quad drawing.
        //Load Shaders
        let vertex_shader = if &config.shader_directory != "default" {
            ShaderModule::new_from_spv(
                device.clone(),
                &(config.shader_directory.clone() + "icon/vert.spv"),
            ).expect("Failed to load vertex shader: rectangle")
        }else{
            ShaderModule::new_from_code(
                device.clone(),
                default_icon_vertex_shader()
            ).expect("Failed to load vertex shader: rectangle: from code")
        };
        let fragment_shader = if &config.shader_directory != "default" {
            ShaderModule::new_from_spv(
                device.clone(),
                &(config.shader_directory.clone() + "icon/frag.spv"),
            ).expect("Failed to load fragment shader: rectangle")
        }else{
            ShaderModule::new_from_code(
                device.clone(),
                default_icon_frag_shader()
            ).expect("Failed to load fragment shader: rectangle: from code")    
        };

        let vertex_stage = vertex_shader.to_stage(Stage::Vertex, "main");
        let fragment_stage = fragment_shader.to_stage(Stage::Fragment, "main");

        // setup Graphicspipeline
        //This one descibes how vertices are bound to the shader.
        let vertex_state = get_default_vertex_state();
        //This describes how the input (the vertices) should be interpreted
        let input_assembly_state = InputAssemblyState {
            topology: vk::PrimitiveTopology::TRIANGLE_LIST,
            restart_enabled: false,
        };

        //raster_info
        let raster_info = RasterizationState {
            depth_clamp_enabled: false,
            rasterizer_discard_enabled: false,
            polygone_mode: vk::PolygonMode::FILL,
            cull_mode: vk::CullModeFlags::NONE, //No culling for now
            front_face: vk::FrontFace::COUNTER_CLOCKWISE,
            line_width: None,
            depth_bias: None, //No depth bias for now
        };

        //TODO multisample_state
        let multisample_state = MultisampleState {
            sample_count: vk::SampleCountFlags::TYPE_1,
            sample_shading: false,
            min_sample_shading: 1.0,
            alpha_to_coverage: false,
            alpha_to_one: false,
        };
        //TODO depth_stencil_state
        let depth_stencil_state = DepthStencilState {
            depth_test_enabled: true,
            depth_write_enabled: true,
            depth_compare_op: vk::CompareOp::LESS,
            stencil_test: StencilTestState::NoTest,
            depth_bounds: None,
        };
        //Blend with everything we get
        let color_blend_state = ColorBlendState::new(
            Some([0.0, 0.0, 0.0, 0.0]),
            None,
            vec![
                //Our Color attachment, we don't blend, but discard in the shader if the alpha value is below 1.0
                ColorBlendAttachmentState::Blending {
                    //selects which blend factor is used to determine the source factors
                    src_color_blend_factor: vk::BlendFactor::SRC_ALPHA,
                    //selects which blend factor is used to determine the destination factors
                    dst_color_blend_factor: vk::BlendFactor::ONE_MINUS_SRC_ALPHA,
                    // set the blend operation used to write to this attachment
                    color_blend_op: vk::BlendOp::ADD,
                    // selects which blend factor is used to determine the source factor
                    src_alpha_blend_factor: vk::BlendFactor::ONE,
                    // selects which blend factor is used to determine the destination factor
                    dst_alpha_blend_factor: vk::BlendFactor::ZERO,
                    //the blending operation for the alpha channel.
                    alpha_blend_op: vk::BlendOp::ADD,
                    //Mask which describes which channels are enabled for writing.
                    color_write_mask: vk::ColorComponentFlags::all(),
                },
            ],
        );

        //Create pipeline
        let pipeline_state = PipelineState::new(
            vertex_state,
            input_assembly_state,
            None,
            ViewportState::new(ViewportMode::Dynamic(1)), //We have no viewport state since we set the viewport at runtime
            raster_info,
            multisample_state,
            depth_stencil_state,
            Some(color_blend_state),
        );
        
        //Create a fake push constant to have some range
        let fake_push = PushConstant::new(
            IconPush {
                transform: [[0.0; 4]; 4],
                loc_extent: [0.0;4],
                level: 0.0,
            },
            vk::ShaderStageFlags::VERTEX | vk::ShaderStageFlags::FRAGMENT,
        );

        //Setup the persistent descriptor sets as well as the final pipeline layout, all of the primitives will use
        let pipeline_layout = marp::pipeline::PipelineLayout::new(
            device.clone(),
            desc_set_layouts,
            vec![*fake_push.range()],
        ).expect("Failed to create GraphicsPipelineLayout for Rectangle pipeline");

        let pipeline = GraphicsPipeline::new(
            device.clone(),
            vec![vertex_stage.clone(), fragment_stage.clone()],
            pipeline_state,
            pipeline_layout,
            render_pass,
            0, //Use subpass 0
        ).expect("Failed to create Graphics pipeline");

        Icon {
            index_buffer: self.index_buffer,
            vertex_buffer: self.vertex_buffer,
            pipeline,
            image_and_sampler: self.image_and_sampler,
        }
    }
}

///The Icon primitive can draw a icon based on a icons theme.
pub struct Icon {
    index_buffer: Arc<Buffer>,
    vertex_buffer: Arc<Buffer>,
    ///Renders a triangle on which the icon is drawn
    pipeline: Arc<GraphicsPipeline>,
    //Holds the image and the sampler for the icons
    image_and_sampler: (Arc<Image>, Arc<Sampler>)
}


impl Icon {
    pub fn get_image_and_sampler(&self) -> (Arc<Image>, Arc<Sampler>){
        self.image_and_sampler.clone()
    }

    pub fn get_pipeline_layout(&self) -> Arc<PipelineLayout> {
        self.pipeline.layout()
    }
}

impl PrimDrawable for Icon {
    fn draw(&self, command_buffer: Arc<CommandBuffer>, call: Vec<PrimitiveCall>, renderer: &Renderer) {
        //First bind the pipeline, index buffer and vertex buffer, then draw all calls.
        command_buffer
            .cmd_bind_pipeline(vk::PipelineBindPoint::GRAPHICS, self.pipeline.clone())
            .expect("Failed to bind Rect pipeline!");

        command_buffer
            .cmd_bind_index_buffer(
                self.index_buffer.clone(),
                0, //No offset into the buffer
                vk::IndexType::UINT32,
            )
            .expect("Failed to bind Rect Index Buffer!");

        command_buffer
            .cmd_bind_vertex_buffers(0, vec![(self.vertex_buffer.clone(), 0)])
            .expect("Failed to bind vertex buffer");
        
        for p in call{
            if let Primitive::Icon(ref ico) = p.prim{
                //Setup the scissors for this call
                command_buffer
                    .cmd_set_scissors(vec![p.get_scissors().to_scissors()])
                    .expect("Failed to set scissors!");
                
                let pc_vert = PushConstant::new(
                    IconPush {
                        transform: p.get_transform(renderer.screen_area()),
                        loc_extent: ico.get_location_and_extent(),
                        level: p.get_level()
                    },
                    vk::ShaderStageFlags::VERTEX | vk::ShaderStageFlags::FRAGMENT,
                );

                //Bind the current correct push const
                command_buffer
                    .cmd_push_constants(self.pipeline.layout(), &pc_vert)
                    .expect("Failed to push triangles constants");

                command_buffer
                    .cmd_draw_indexed(6, 1, 0, 0, 1)
                    .expect("Failed to draw rect!");    
            }
        }
    }
}

///The default icon push block
#[repr(C)]
pub struct IconPush{
    pub transform: [[f32;4];4],
    //The 2D location and extent of that icons area on the icon map
    pub loc_extent: [f32;4],
    //The level at which the primitive is drawn
    pub level: f32,
}
