/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */







///The image primitive can draw a single image to an area.
/// The difference to the icon is, that it can set its own image.
/// # Safety
/// The supplied image has to be in `SHADER_READ_OPTIMAL` or `GENERAL` layout
/// and must support to be bind as an uniform sampled image.
pub struct PrimImage {
    //The currently used descriptor pool. Might change if insufficient
    //descriptor_pool: RwLock<Arc<DescriptorPool>>
}

//Implementation
// Have descriptor pool,
// Grow pool based on the max amount of images used in one frame
// Attach all images to binding 0
// Attach all drawing info to binding 1
// Push drawing count on PC's
