/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use marp::ash::vk;
use marp::command_buffer::*;
use marp::pipeline::*;
use marp::*;
use std::mem;
use std::sync::Arc;

use crate::rendering::icon::*;
use crate::rendering::{ColorTricks, Renderer};
use crate::widget::*;

#[derive(Clone)]
pub enum Primitive {
    Rect,
    ///Draws a line along the set of points supplied. The coordinates are expressed within the area of the PrimitiveCall.
    /// So a point of (0,0) will be in the upper left of the `area` of PrimitiveCall.
    Line {
        point_set: Vec<(f64, f64)>,
        line_width: f32,
        color: [f32;4]
    },
    /// Writes the string to the supplied area. If characters are too big to fit in, they are cut away.
    Text {
        ///The string that should be written
        content: String,
        ///The font size in pixel
        size: u32,
    },
    /// Draws the supplied image to the `area` of the PrimitiveCall
    Image(Arc<image::Image>),
    /// Draws the specified Icon to the `area` of the PrimitiveCall
    Icon(Ico),
}

#[derive(Clone, Debug)]
pub enum Theme{
    ///For unused space.
    Pattern,
    ///Background of unfocused/passive elements.
    Background,
    ///Background for focused elements
    BackgroundHighlight,
    ///Highlighting pattern for interface edges.
    EdgePattern,
    ///Default text color
    Text,
    ///Default color of text highlights
    TextHighlight,
    ///Default text cursor color
    TextCursor,
}

impl Theme{
    pub fn to_color(&self, renderer: &Renderer) -> [f32;4]{
        match self{
            Theme::Pattern => renderer.pallet().background_color,
            Theme::Background => renderer.pallet().background_color,
            Theme::BackgroundHighlight => renderer.pallet().background_color.highlight(renderer.pallet().inc),
            Theme::EdgePattern => renderer.pallet().edge_color,
            Theme::Text => renderer.pallet().text_color,
            Theme::TextHighlight => renderer.pallet().highlight_color,
            Theme::TextCursor => renderer.pallet().text_color.highlight(renderer.pallet().inc)
        }
    }
    pub fn to_pattern(&self, renderer: &Renderer) -> ([f32;4], [f32;4]){
        match self{
            Theme::Pattern => (
                renderer.pallet().background_color,
                renderer.pallet().background_color.darken(renderer.pallet().inc)
            ),
            Theme::Background => (
                renderer.pallet().background_color,
                renderer.pallet().background_color
            ),
            Theme::BackgroundHighlight => (
                renderer.pallet().background_color.highlight(renderer.pallet().inc * 2.0),
                renderer.pallet().background_color.highlight(renderer.pallet().inc * 2.0)
            ),
            Theme::EdgePattern => (
                renderer.pallet().edge_color,
                renderer.pallet().edge_color.highlight(renderer.pallet().inc)
            ),
            Theme::Text => (
                renderer.pallet().text_color,
                renderer.pallet().text_color
            ),
            Theme::TextHighlight => (
                renderer.pallet().highlight_color,
                renderer.pallet().highlight_color
            ),
            Theme::TextCursor => (
                renderer.pallet().text_color.highlight(renderer.pallet().inc),
                renderer.pallet().text_color.highlight(renderer.pallet().inc)
            )
        }
    }
}

#[derive(Clone, Debug)]
pub enum DrawType{
    Color([f32;4]),
    Theme(Theme)
}

impl DrawType{
    pub fn to_color(&self, renderer: &Renderer) -> [f32;4]{
        match self{
            DrawType::Color(c) => *c,
            DrawType::Theme(t) => t.to_color(renderer)
        }
    }
    ///Returns two pattern colors, if a pattern is used. Otherwise returns two times the same color which will turn the surface into a clean
    /// color. The pattern is currently only usable with rect primitive.
    pub fn to_pattern(&self, renderer: &Renderer) -> ([f32;4], [f32;4]){
        match self{
            DrawType::Color(c) => (*c, (*c).highlight(renderer.pallet().inc)),
            DrawType::Theme(t) => t.to_pattern(renderer)
        }
    }
}

///Defines which primitive has to be drawn where.
#[derive(Clone)]
pub struct PrimitiveCall {
    pub prim: Primitive,
    /// Most Primitives are more or less boxes, this marks origin and extent of this box.
    pub area: Area,
    /// This marks an absolute scissors area. So the offset is not take into consideration. The feature is mostly used when a widget is placed within a viewport.
    pub scissors_area: Option<Area>,
    /// This is the level of this primitive. If the level is 0 it is the lowest.
    /// If you want to draw on top the value has to be higher.
    pub level: u32,
    pub draw_type: DrawType,
}

impl PrimitiveCall {
    ///Initiates the primitive call with no offset and the scissors area set the the `area`.
    pub fn new(
        primitive: Primitive,
        area: Area,
        level: u32,
        draw_type: DrawType
    ) -> Self{
        PrimitiveCall{
            prim: primitive,
            area,
            scissors_area: None,
            level,
            draw_type
        }
    }

    ///Sets a special scissors area for this call.
    pub fn set_scissors_area(&mut self, scissors_area: Area){
        self.scissors_area = Some(scissors_area);
    }
    
    ///Returns the "height" at which the element should be drawn
    pub fn get_level(&self) -> f32 {
        //Special case where we don't want the 1.0 since that gets clipped
        if self.level == 0{
            return 0.9;
        }
        1.0 / (self.level + 1) as f32
    }

    ///Returns the transform matrix for this call. Might be different due to offsets set.
    pub fn get_transform(&self, screen_area: &Area) -> [[f32;4];4]{
        self.area.to_transform(screen_area)
    }

    ///Returns the correct scissors for this call
    pub fn get_scissors(&self) -> Area{
        if let Some(ref sc) = self.scissors_area{
            *sc
        }else{
            self.area
        }
    }
}

///Defines a trait which should make an object drawable when supplied with a command buffer.
/// * TODO: * add documentation which push constants can be set to impact the drawing.
pub trait PrimDrawable {
    ///Assumes that a command buffer is supplied that is in a Renderpass that writes to a single 4-channel image on binding=0.
    fn draw(&self, command_buffer: Arc<CommandBuffer>, call: Vec<PrimitiveCall>, renderer: &Renderer);
}

///Defines a normal vertex which can be supplied to the standard  renderpass and pipeline used by the
/// renderer.
#[derive(Clone, Copy, Debug)]
#[repr(C)]
pub struct Vertex {
    pub pos: [f32; 4],
    pub uv: [f32; 2],
}

///Returns the vertex state of `Vertex` which can be used to build a graphical pipeline which uses
///these vertices as input.
pub fn get_default_vertex_state() -> VertexInputState {
    //This one descibes how vertices are bound to the shader.
    pipeline::VertexInputState {
        vertex_binding_descriptions: vec![vk::VertexInputBindingDescription::builder()
            .binding(0)
            .stride(mem::size_of::<Vertex>() as u32)
            .input_rate(vk::VertexInputRate::VERTEX)
            .build()],
        vertex_attribute_descriptions: vec![
            //Description of the Pos attribute
            vk::VertexInputAttributeDescription::builder()
                .location(0)
                .binding(0)
                .format(vk::Format::R32G32B32A32_SFLOAT)
                .offset(offset_of!(Vertex, pos) as u32)
                .build(),
            //Description of the uv attribute
            vk::VertexInputAttributeDescription::builder()
                .location(1)
                .binding(0)
                .format(vk::Format::R32G32_SFLOAT)
                .offset(offset_of!(Vertex, uv) as u32)
                .build(),
        ],
    }
}
