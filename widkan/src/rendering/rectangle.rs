/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use marp::buffer::*;
use marp::command_buffer::*;
use marp::descriptor::*;
use marp::device::*;
use marp::framebuffer::RenderPass;
use marp::memory::*;
use marp::pipeline::*;
use marp::shader::*;

use marp::ash::vk;

use crate::config::Config;
use crate::rendering::primitive::*;
use crate::rendering::Renderer;
use std::sync::Arc;

use std::u64;

fn default_rect_vertex_shader() -> Vec<u32>{
    let bytes = include_bytes!("../../resources/shader/rectangle/vert.spv");
    marp::ash::util::read_spv(&mut std::io::Cursor::new(&bytes[..])).expect("Could not read rectangle shader!")
}

fn default_rect_frag_shader() -> Vec<u32>{
    let bytes = include_bytes!("../../resources/shader/rectangle/frag.spv");
    marp::ash::util::read_spv(&mut std::io::Cursor::new(&bytes[..])).expect("Could not rectanlge line shader!")
}


pub struct Rectangle {
    index_buffer: Arc<Buffer>,
    vertex_buffer: Arc<Buffer>,
    ///The pipeline that renders a single rectangle.
    pipeline: Arc<GraphicsPipeline>,
}

impl Rectangle {
    ///Creates a new Rectangle which can be drawn when a command buffer is supplied
    pub fn new(
        config: &Config,
        device: Arc<Device>,
        init_queue: Arc<Queue>,
        render_pass: Arc<RenderPass>,
    ) -> Self {
        let indices: Vec<u32> = vec![0, 1, 2, 2, 3, 0];
        let (index_upload, index_buffer) = Buffer::from_data(
            device.clone(),
            init_queue.clone(),
            indices.clone(), //The buffer size we need
            BufferUsage {
                index_buffer: true,
                storage_buffer: true,
                ..Default::default()
            },
            SharingMode::Exclusive,
            MemoryUsage::GpuOnly //we only want device access for this buffer.
        )
        .expect("Failed to create index buffer");
        index_upload.wait(std::u64::MAX).expect("Failed to wait for index upload!");
        //We go from 0.0 to 1.0, since we then can scale appropriate and afterwards move the rectangle to the right position
        let vertices = vec![
            Vertex {
                pos: [0.0, 1.0, 0.0, 1.0],
                uv: [0.0, 1.0],
            },
            Vertex {
                pos: [1.0, 1.0, 0.0, 1.0],
                uv: [1.0, 1.0],
            },
            Vertex {
                pos: [1.0, 0.0, 0.0, 1.0],
                uv: [1.0, 0.0],
            },
            Vertex {
                pos: [0.0, 0.0, 0.0, 1.0],
                uv: [0.0, 0.0],
            },
        ];

        let (vertex_upload, vertex_buffer) = Buffer::from_data(
            device.clone(),
            init_queue.clone(),
            vertices,
            BufferUsage {
                vertex_buffer: true,
                ..Default::default()
            },
            SharingMode::Exclusive,
            MemoryUsage::GpuOnly, //we only want device access for this buffer.
        )
        .expect("Failed to create index buffer");

        vertex_upload.wait(std::u64::MAX).expect("Failed to wait for vertex upload!");

        //Create the pipeline that handles the quad drawing.
        //Load Shaders
        let vertex_shader = if &config.shader_directory != "default" {
            ShaderModule::new_from_spv(
                device.clone(),
                &(config.shader_directory.clone() + "rectangle/vert.spv"),
            ).expect("Failed to load vertex shader: rectangle")
        }else{
            ShaderModule::new_from_code(
                device.clone(),
                default_rect_vertex_shader()
            ).expect("Failed to load default rect shader!")
        };
        
        let fragment_shader = if &config.shader_directory != "default" {
            ShaderModule::new_from_spv(
            device.clone(),
            &(config.shader_directory.clone() + "rectangle/frag.spv"),
        ).expect("Failed to load fragment shader: rectangle")
        }else{
            ShaderModule::new_from_code(
                device.clone(),
                default_rect_frag_shader()
            ).expect("Failed to load default rect shader!")
        };

        let vertex_stage = vertex_shader.to_stage(Stage::Vertex, "main");
        let fragment_stage = fragment_shader.to_stage(Stage::Fragment, "main");

        // setup Graphicspipeline
        //This one descibes how vertices are bound to the shader.
        let vertex_state = get_default_vertex_state();
        //This describes how the input (the vertices) should be interpreted
        let input_assembly_state = InputAssemblyState {
            topology: vk::PrimitiveTopology::TRIANGLE_LIST,
            restart_enabled: false,
        };

        //raster_info
        let raster_info = RasterizationState {
            depth_clamp_enabled: false,
            rasterizer_discard_enabled: false,
            polygone_mode: vk::PolygonMode::FILL,
            cull_mode: vk::CullModeFlags::NONE, //No culling for now
            front_face: vk::FrontFace::COUNTER_CLOCKWISE,
            line_width: None,
            depth_bias: None, //No depth bias for now
        };

        //TODO multisample_state
        let multisample_state = MultisampleState {
            sample_count: vk::SampleCountFlags::TYPE_1,
            sample_shading: false,
            min_sample_shading: 1.0,
            alpha_to_coverage: false,
            alpha_to_one: false,
        };
        //TODO depth_stencil_state
        let depth_stencil_state = DepthStencilState {
            depth_test_enabled: true,
            depth_write_enabled: true,
            depth_compare_op: vk::CompareOp::LESS,
            stencil_test: StencilTestState::NoTest,
            depth_bounds: None,
        };
        //Blend with everything we get
        let color_blend_state = ColorBlendState::new(
            Some([0.0, 0.0, 0.0, 0.0]),
            Some(vk::LogicOp::COPY),
            vec![
                //Our Color attachment, we don't blend, but discard in the shader if the alpha value is below 1.0
                ColorBlendAttachmentState::NoBlending,
            ],
        );

        //Create pipeline
        let pipeline_state = PipelineState::new(
            vertex_state,
            input_assembly_state,
            None,
            ViewportState::new(ViewportMode::Dynamic(1)), //We have no viewport state since we set the viewport at runtime
            raster_info,
            multisample_state,
            depth_stencil_state,
            Some(color_blend_state),
        );

        //Create a fake push constant to have some range
        let fake_push = PushConstant::new(
            RectPush {
                transform: [[0.0; 4]; 4],
                level: 1.0,
                color1: [1.0;4],
                color2: [1.0;4],
                pad1: [1.0; 3]
            },
            vk::ShaderStageFlags::VERTEX | vk::ShaderStageFlags::FRAGMENT,
        );

        //Setup the persistent descriptor sets as well as the final pipeline layout, all of the primitives will use
        let pipeline_layout = marp::pipeline::PipelineLayout::new(
            device.clone(),
            vec![],
            vec![*fake_push.range()],
        )
        .expect("Failed to create GraphicsPipelineLayout for Rectangle pipeline");

        let pipeline = GraphicsPipeline::new(
            device.clone(),
            vec![vertex_stage.clone(), fragment_stage.clone()],
            pipeline_state,
            pipeline_layout,
            render_pass,
            0, //Use subpass 0
        )
        .expect("Failed to create Graphics pipeline");

        Rectangle {
            index_buffer,
            vertex_buffer,
            pipeline,
        }
    }

    pub fn get_pipeline_layout(&self) -> Arc<PipelineLayout> {
        self.pipeline.layout()
    }

    pub fn get_pipeline(&self) -> &Arc<GraphicsPipeline>{
        &self.pipeline
    }
}

impl PrimDrawable for Rectangle {
    fn draw(&self, command_buffer: Arc<CommandBuffer>, call: Vec<PrimitiveCall>, renderer: &Renderer) {
        //First bind the pipeline, index buffer and vertex buffer, then draw all calls.
        command_buffer
            .cmd_bind_pipeline(vk::PipelineBindPoint::GRAPHICS, self.pipeline.clone())
            .expect("Failed to bind Rect pipeline!");

        command_buffer
            .cmd_bind_index_buffer(
                self.index_buffer.clone(),
                0, //No offset into the buffer
                vk::IndexType::UINT32,
            )
            .expect("Failed to bind Rect Index Buffer!");

        command_buffer
            .cmd_bind_vertex_buffers(0, vec![(self.vertex_buffer.clone(), 0)])
            .expect("Failed to bind vertex buffer");

        for p in call{

            //Setup the scissors for this call
            command_buffer
                .cmd_set_scissors(vec![p.get_scissors().to_scissors()])
                .expect("Failed to set scissors!");

            let (col1, col2) = p.draw_type.to_pattern(renderer);
            let pc_vert = PushConstant::new(
                RectPush {
                    transform: p.get_transform(renderer.screen_area()),
                    color1: col1,
                    color2: col2,
                    level: p.get_level(),
                    pad1: [0.0;3],
                },
                vk::ShaderStageFlags::VERTEX | vk::ShaderStageFlags::FRAGMENT,
            );

            //Bind the current correct push const
            command_buffer
                .cmd_push_constants(self.pipeline.layout(), &pc_vert)
                .expect("Failed to push triangles constants");

            command_buffer
                .cmd_draw_indexed(6, 1, 0, 0, 1)
                .expect("Failed to draw rect!");
        }
    }
}

///The default vertex buffer push block
#[repr(C)]
pub struct RectPush {
    pub transform: [[f32; 4]; 4],
    pub color1: [f32;4],
    pub color2: [f32;4],
    pub level: f32,
    pub pad1: [f32;3],
}
