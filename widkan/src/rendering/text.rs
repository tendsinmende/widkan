/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use marp::ash::vk;
use marp::buffer::*;
/// The `Text` primitive can render a text into the `area` supplied with the `PrimitiveCall`.
/// The text is based on some font supplied at creation time of the Text Primitive.
use marp::command_buffer::*;
use marp::descriptor::*;
use marp::device::*;
use marp::framebuffer::RenderPass;
use marp::image::*;
use marp::memory::*;
use marp::pipeline::*;
use marp::sampler::*;
use marp::shader::*;

use image;
use rusttype::*;

use crate::config::Config;
use crate::rendering::primitive::*;
use crate::rendering::Renderer;
use crate::widget::Area;
use std::sync::Arc;
use std::u64;

use std::collections::HashMap;
use std::io::Read;


fn default_text_vertex_shader() -> Vec<u32>{
    let bytes = include_bytes!("../../resources/shader/text/vert.spv");
    marp::ash::util::read_spv(&mut std::io::Cursor::new(&bytes[..])).expect("Could not read text shader!")
}

fn default_text_frag_shader() -> Vec<u32>{
    let bytes = include_bytes!("../../resources/shader/text/frag.spv");
    marp::ash::util::read_spv(&mut std::io::Cursor::new(&bytes[..])).expect("Could not read text shader!")
}

fn default_font() -> &'static [u8]{
    let bytes = include_bytes!("../../resources/fonts/Roboto-Regular.ttf");
    bytes
}

///Holds information where on the bitmap this character is.
pub struct Character {
    ///The character
    pub character: char,
    ///The uv location on the generated bitmap on the gpu.
    pub min_max: [f32; 4],
    ///The pixel location
    pos: (f32, f32),
    ///The pixel extent
    extent: (f32, f32),
    ///The font size at which the character was rendered
    font_size: u32,
    ///the H_Metrics of this character
    h_metrics: HMetrics,
    /// The offset "down" at which the character has to be placed.
    ///For instance the "A" offset might be 0, then the "a" might be 3px, since it has to be possitioned a little lower
    y_offset: f32,
}

impl Character {
    ///Returns the width and high of this character for a supplied font size
    fn width_height(&self, size: u32) -> (f32, f32) {
        (
            (self.extent.0 / self.font_size as f32) * size as f32,
            (self.extent.1 / self.font_size as f32) * size as f32,
        )
    }

    ///Returns the offset in x direction, at which the next glyph can be drawn after this one
    pub fn offset_to_next(&self, scale: u32) -> f32 {
        (self.h_metrics.advance_width / self.font_size as f32) * scale as f32
    }

    ///Returns the offset this glyph has to be moved "Down" compared to (0.0,0.0) in the top left, top be positioned "right".
    pub fn offset_down(&self, scale: u32) -> f32 {
        (self.y_offset / self.font_size as f32) * scale as f32
    }
}

pub struct CharacterStore {
    pub characters: HashMap<char, Character>,
    v_metrics: VMetrics,
    ///The font size at which this store was rendered.
    font_size: u32,

    ///The maximum extent a character can have
    pub max_extent: (f32, f32),
}

impl CharacterStore {
    ///Initialises the bitmap and a hashmap which knows where each character is laid out.
    /// Returns self and the image that needs to be uploaded to the gpu.
    pub fn new(config: &Config) -> (Self, image::RgbaImage) {

        //load a config font, or some specified one
        let buffer: Vec<u8> = if &config.font_path != "default"{
            let mut file = std::fs::File::open(&config.font_path)
                .expect(&format!("Could not open font as: {}", &config.font_path));

            let mut buf = Vec::new();
            file.read_to_end(&mut buf).expect(&format!(
                "failed to read data from font at: {}",
                &config.font_path
            ));
            buf
        }else{
            default_font().to_vec()
        };

        //load the font
        let font = Font::try_from_bytes(&buffer)
            .expect(&format!("Could not load font from {}", config.font_path));

        //Setup the size we want to have rendered
        let scale = Scale::uniform(config.font_quality as f32);
        //Our test text is the whole alphabet as well as the most common symbols
        let all_chars = config.font_characters.clone();
        let v_metrics = font.v_metrics(scale);

        //For each id, generate a glyph
        let glyphs: Vec<(char, PositionedGlyph)> = all_chars
            .chars()
            .map(|c| (c, font.glyph(c).scaled(scale).positioned(point(0.0, 0.0))))
            .collect();
        // work out the layout size
        //we need the max_height to knwo how much we have to offset a character "down" so in +y when drawing it
        // relative to the highes character.
        let (glyphs_width, glyphs_height, max_height) = {
            let (min_x, max_x, min_y, max_y, max_h) = glyphs.iter().fold(
                (
                    std::i32::MAX,
                    std::i32::MIN,
                    std::i32::MAX,
                    std::i32::MIN,
                    std::i32::MIN,
                ),
                |(min_x, max_x, min_y, max_y, max_height), (c, x)| {
                    let bbox = x.pixel_bounding_box().expect(&format!(
                        "Could not get pixel bounding box for char: {} / {:?}",
                        c, x.id()
                    ));

                    let new_min_x = min_x.min(bbox.min.x);
                    let new_max_x = max_x.max(bbox.max.x);

                    let new_min_y = min_y.min(bbox.min.y);
                    let new_max_y = max_y.max(bbox.max.y);

                    let new_max_height = max_height.max(bbox.min.y.abs());

                    (new_min_x, new_max_x, new_min_y, new_max_y, new_max_height)
                },
            );

            ((max_x - min_x), (max_y - min_y), max_h)
        };

        let glyphs_per_axis = (all_chars.len() as f64).sqrt().ceil() as u32;
        /*
                println!("Char: {}, {}", glyphs_width, glyphs_height);
                println!("Size: {},{}",
                         glyphs_per_axis as i32 * glyphs_width,
                         glyphs_per_axis * glyphs_height
                );
        */
        let img_extent = (
            glyphs_per_axis * glyphs_width as u32,
            glyphs_per_axis * glyphs_height as u32,
        );
        // Create a new rgba image with some padding
        let mut image = image::DynamicImage::new_rgba8(img_extent.0, img_extent.1).to_rgba();

        let mut characters: HashMap<char, Character> = HashMap::with_capacity(all_chars.len());
        // Loop through the glyphs, render each to its position and gen the Character struct
        for (idx, (ch, glyph)) in glyphs.iter().enumerate() {
            //Calc the x and y coords of this glyph
            let ox = (idx as u32 % glyphs_per_axis) * glyphs_width as u32;
            let oy = (idx as u32 / glyphs_per_axis) * glyphs_height as u32;

            glyph.draw(|x, y, v| {
                image.put_pixel(
                    // Offset the position by the glyph bounding box
                    x + ox,
                    y + oy,
                    // Turn the coverage into an alpha value
                    image::Rgba::<_>([0, 0, 0, (v * 255.0) as u8]),
                )
            });

            //TODO Actually generate the character which knows where on the map it is.
            let pos = ((ox as f32), (oy as f32));
            let bbox = glyph.pixel_bounding_box().expect(&format!(
                "Could not get pixel bounding box for {:?}",
                glyph.id()
            ));

            let extent = (
                (bbox.max.x - bbox.min.x) as f32,
                (bbox.max.y - bbox.min.y) as f32,
            );
            /*
            println!("x: {}, y: {}\n\tpos: {:?}\n\textent: {:?}", ox, oy, pos, extent);
            */
            let uvs = [
                pos.0 / img_extent.0 as f32,
                pos.1 / img_extent.1 as f32,
                (pos.0 + extent.0) / img_extent.0 as f32,
                (pos.1 + extent.1) / img_extent.1 as f32,
            ];

            let _ = characters.insert(
                *ch,
                Character {
                    character: *ch,
                    min_max: uvs,
                    pos,
                    extent,
                    font_size: config.font_quality,
                    h_metrics: glyph.unpositioned().h_metrics(),
                    y_offset: ((bbox.min.y + max_height) as f32),
                },
            );
        }

        //image.save("Glyphs.png");

        (
            CharacterStore {
                characters,
                v_metrics,
                font_size: config.font_quality,
                max_extent: (glyphs_width as f32, glyphs_height as f32),
            },
            image,
        )
    }

    pub fn get_max_extent(&self, scale: u32) -> (f32, f32) {
        (
            (self.max_extent.0 / self.font_size as f32) * scale as f32,
            (self.max_extent.1 / self.font_size as f32) * scale as f32,
        )
    }
}

pub struct TextBuilder{
    index_buffer: Arc<Buffer>,
    vertex_buffer: Arc<Buffer>,
    character_store: CharacterStore,
    image_and_sampler: (Arc<Image>, Arc<Sampler>),    
}

impl TextBuilder{
    pub fn new(
        config: &Config,
        device: Arc<Device>,
        init_queue: Arc<Queue>,
    ) -> Self{
        let indices: Vec<u32> = vec![0, 1, 2, 2, 3, 0];
        let (index_upload, index_buffer) = marp::buffer::Buffer::from_data(
            device.clone(),
            init_queue.clone(),
            indices.clone(), //The buffer size we need
            BufferUsage {
                index_buffer: true,
                storage_buffer: true,
                ..Default::default()
            },
            SharingMode::Exclusive,
            MemoryUsage::GpuOnly, //we only want device access for this buffer.
        )
        .expect("Failed to create index buffer");
        index_upload
            .wait(u64::MAX)
            .expect("Failed to upload inidice buffer!");
        //We go from 0.0 to 1.0, since we then can scale appropriate and afterwards move the rectangle to the right position
        let vertices = vec![
            Vertex {
                pos: [0.0, 1.0, 0.0, 1.0],
                uv: [0.0, 1.0],
            },
            Vertex {
                pos: [1.0, 1.0, 0.0, 1.0],
                uv: [1.0, 1.0],
            },
            Vertex {
                pos: [1.0, 0.0, 0.0, 1.0],
                uv: [1.0, 0.0],
            },
            Vertex {
                pos: [0.0, 0.0, 0.0, 1.0],
                uv: [0.0, 0.0],
            },
        ];

        let (vertex_upload, vertex_buffer) = Buffer::from_data(
            device.clone(),
            init_queue.clone(),
            vertices, //The buffer size we need
            BufferUsage {
                vertex_buffer: true,
                ..Default::default()
            },
            SharingMode::Exclusive,
            MemoryUsage::GpuOnly, //we only want device access for this buffer.
        )
        .expect("Failed to create index buffer");
        vertex_upload
            .wait(u64::MAX)
            .expect("Failed to upload vertex buffer!");

        //Load the image and build a descriptor set for it.
        let (char_store, upload_image) = CharacterStore::new(config);

        let image_dimensions = upload_image.dimensions();
        let image_data = upload_image.into_raw();

        let (image_upload, image) = Image::from_data(
            device.clone(),
            init_queue.clone(),
            ImageInfo::new(
                ImageType::Image2D {
                    width: image_dimensions.0,
                    height: image_dimensions.1,
                    samples: 1,
                },
                vk::Format::R8G8B8A8_UNORM,
                None,                        //No extra ordinary component mapping
                Some(MipLevel::Specific(1)), //Only one mip map for now
                ImageUsage {
                    sampled: true,
                    //transfer_dst: true,
                    color_attachment: true,
                    ..Default::default()
                },
                MemoryUsage::GpuOnly,
                None, //Optimal tiling by default
            ),
            SharingMode::Exclusive,
            image_data,
        )
        .expect("Failed to create image");
        image_upload
            .wait(u64::MAX)
            .expect("Failed to wait for image upload!");

        //Create a smapler which will be used to sample from the image.
        //TODO optimize for sharpness
        let sampler = SamplerBuilder::default()
            .build(device.clone())
            .expect("Failed to create sampler!");

        TextBuilder{
            vertex_buffer,
            index_buffer,
            character_store: char_store,
            image_and_sampler: (image, sampler)
        }

    }

    pub fn get_image_and_sampler(&self) -> (Arc<Image>, Arc<Sampler>){
        self.image_and_sampler.clone()
    }

    pub fn build(self,
                 config: &Config,
                 device: Arc<Device>,
                 _init_queue: Arc<Queue>,
                 render_pass: Arc<RenderPass>,
                 static_desc_set_layouts: Vec<vk::DescriptorSetLayout>
    ) -> Text{
        
        //Create the pipeline that handles the quad drawing.
        //Load Shaders
        let vertex_shader = if &config.shader_directory != "default" {
            ShaderModule::new_from_spv(
                device.clone(),
                &(config.shader_directory.clone() + "text/vert.spv"),
            ).expect("Failed to load vertex shader: text")
        }else{
            ShaderModule::new_from_code(
                device.clone(),
                default_text_vertex_shader()
            ).expect("Failed to load default text shader!")
        };
        
        let fragment_shader = if &config.shader_directory != "default" {
            ShaderModule::new_from_spv(
            device.clone(),
            &(config.shader_directory.clone() + "text/frag.spv"),
        ).expect("Failed to load fragment shader: text")
        }else{
            ShaderModule::new_from_code(
                device.clone(),
                default_text_frag_shader()
            ).expect("Failed to load default text shader!")
        };

        let vertex_stage = vertex_shader.to_stage(Stage::Vertex, "main");
        let fragment_stage = fragment_shader.to_stage(Stage::Fragment, "main");

        // setup Graphicspipeline
        //This one descibes how vertices are bound to the shader.
        let vertex_state = get_default_vertex_state();
        //This describes how the input (the vertices) should be interpreted
        let input_assembly_state = InputAssemblyState {
            topology: vk::PrimitiveTopology::TRIANGLE_LIST,
            restart_enabled: false,
        };
        //raster_info
        let raster_info = RasterizationState {
            depth_clamp_enabled: false,
            rasterizer_discard_enabled: false,
            polygone_mode: vk::PolygonMode::FILL,
            cull_mode: vk::CullModeFlags::NONE, //No culling for now
            front_face: vk::FrontFace::COUNTER_CLOCKWISE,
            line_width: None,
            depth_bias: None, //No depth bias for now
        };

        //TODO multisample_state
        let multisample_state = MultisampleState {
            sample_count: vk::SampleCountFlags::TYPE_1,
            sample_shading: false,
            min_sample_shading: 1.0,
            alpha_to_coverage: false,
            alpha_to_one: false,
        };
        //TODO depth_stencil_state
        let depth_stencil_state = DepthStencilState {
            depth_test_enabled: true,
            depth_write_enabled: true,
            depth_compare_op: vk::CompareOp::LESS,
            stencil_test: StencilTestState::NoTest,
            depth_bounds: None,
        };
        //TODO color_blend
        let color_blend_state = ColorBlendState::new(
            Some([0.0, 0.0, 0.0, 0.0]),
            None,
            vec![
                //Our Color attachment, we don't blend, but discard in the shader if the alpha value is below 1.0
                ColorBlendAttachmentState::Blending {
                    //selects which blend factor is used to determine the source factors
                    src_color_blend_factor: vk::BlendFactor::SRC_ALPHA,
                    //selects which blend factor is used to determine the destination factors
                    dst_color_blend_factor: vk::BlendFactor::ONE_MINUS_SRC_ALPHA,
                    // set the blend operation used to write to this attachment
                    color_blend_op: vk::BlendOp::ADD,
                    // selects which blend factor is used to determine the source factor
                    src_alpha_blend_factor: vk::BlendFactor::ONE,
                    // selects which blend factor is used to determine the destination factor
                    dst_alpha_blend_factor: vk::BlendFactor::ZERO,
                    //the blending operation for the alpha channel.
                    alpha_blend_op: vk::BlendOp::ADD,
                    //Mask which describes which channels are enabled for writing.
                    color_write_mask: vk::ColorComponentFlags::all(),
                },
            ],
        );

        //Create pipeline
        let pipeline_state = PipelineState::new(
            vertex_state,
            input_assembly_state,
            None,
            ViewportState::new(ViewportMode::Dynamic(1)), //We have no viewport state since we set the viewport at runtime
            raster_info,
            multisample_state,
            depth_stencil_state,
            Some(color_blend_state),
        );
        
        //Create a fake push constant to have some range
        let fake_push = PushConstant::new(
            TextPush {
                transform: [[0.0; 4]; 4],
                character_mm: [0.0; 4],
                level: 1.0,
                color: [1.0;4],
                pad1: [1.0;3]
            },
            vk::ShaderStageFlags::VERTEX | vk::ShaderStageFlags::FRAGMENT,
        );

        //Setup the persistent descriptor sets as well as the final pipeline layout, all of the primitives will use
        let pipeline_layout = marp::pipeline::PipelineLayout::new(
            device.clone(),
            static_desc_set_layouts,
            vec![*fake_push.range()],
        )
        .expect("Failed to create GraphicsPipelineLayout for Rectangle pipeline");

        let pipeline = GraphicsPipeline::new(
            device.clone(),
            vec![vertex_stage.clone(), fragment_stage.clone()],
            pipeline_state,
            pipeline_layout,
            render_pass,
            0, //Use subpass 0
        )
        .expect("Failed to create Graphics pipeline");

        Text {
            index_buffer: self.index_buffer,
            vertex_buffer: self.vertex_buffer,
            pipeline,
            character_store: self.character_store,
            image_and_sampler: self.image_and_sampler,
        }

    }
}



pub struct Text {
    index_buffer: Arc<Buffer>,
    vertex_buffer: Arc<Buffer>,

    pipeline: Arc<GraphicsPipeline>,

    character_store: CharacterStore,

    image_and_sampler: (Arc<Image>, Arc<Sampler>),
}

impl Text {

    pub fn get_pipeline_layout(&self) -> Arc<PipelineLayout> {
        self.pipeline.layout()
    }

    pub fn get_image_and_sampler(&self) -> (Arc<Image>, Arc<Sampler>){
        self.image_and_sampler.clone()
    }

    ///Returns the length of a given character, Inlcudes the space needed till the next character starts
    pub fn char_length(&self, ch: char, scale: u32) -> f32 {
        match self.character_store.characters.get(&ch) {
            Some(c) => c.offset_to_next(scale),
            None => self.white_space_extent(scale),
        }
    }

    ///Returns the length in pixel of a given sting, if displayed at `size`
    pub fn string_length(&self, string: &str, size: u32) -> f32 {
        let mut len = 0.0;
        for ch in string.chars() {
            match self.character_store.characters.get(&ch) {
                Some(c) => {
                    len += c.offset_to_next(size);
                }
                None => {
                    len += self.white_space_extent(size);
                }
            }
        }

        len
    }

    ///Splits the string into a vec of sub strings, where each sub string fits onto the `area`s x-extent at the given `size`.
    pub fn fit_string(&self, string: String, area: Area, size: u32) -> Vec<String> {
        //So what we do here is, we split the string into "words", get the length of each word and then add until it is
        // "too long" however, the must be at least 1 word per line, since there might single words that a longer then a line.
        // TODO one could handle auto-break "-" thingies... but then I would have to analyze the word for syllables which
        // is a little too much I'd say

        let map: Vec<(f32, String)> = string
            .split(' ')
            .into_iter()
            .map(|substring| {
                //We have to cast into usize here, since f32 is not EQ
                (self.string_length(substring, size), String::from(substring))
            })
            .collect();

        let max_x = area.extent.0 as f32;
        let whitespace_extent = self.white_space_extent(size);
        let mut lines = Vec::new();

        let mut current_length = 0.0;
        let mut current_line = String::new();
        for (len, substring) in map {
            //We always want to have at least one workd per line, but otherwise not "too much"
            if (current_length + len + whitespace_extent) > max_x {
                //Check if the line has not word on it, if so push anyways
                if current_line.len() < 1 {
                    current_line += &(substring + " ");
                    current_length += len + whitespace_extent;
                } else {
                    //Finis this line and add self to the new one
                    lines.push(current_line.clone());
                    current_length = len + whitespace_extent;
                    current_line = substring + " ";
                }
            } else {
                //Thats the normal case of just pushing
                current_line += &(substring + " ");
                current_length += len + whitespace_extent;
            }
        }
        //Push the last line, then return
        lines.push(current_line);

        lines
    }

    pub fn white_space_extent(&self, scale: u32) -> f32 {
        (self.character_store.max_extent.0 / self.character_store.font_size as f32) * (scale as f32)
            / 2.0
    }

    pub fn line_height(&self, scale: u32) -> f32 {
        (self.character_store.max_extent.1 / self.character_store.font_size as f32) * scale as f32
    }
}

impl PrimDrawable for Text {
    fn draw(&self, command_buffer: Arc<CommandBuffer>, call: Vec<PrimitiveCall>, renderer: &Renderer) {
        //return Make more performant by rendering several texsts at once.
        command_buffer
            .cmd_bind_pipeline(vk::PipelineBindPoint::GRAPHICS, self.pipeline.clone())
            .expect("Failed to bind Rect pipeline!");

        command_buffer
            .cmd_bind_index_buffer(
                self.index_buffer.clone(),
                0, //No offset into the buffer
                vk::IndexType::UINT32,
            )
            .expect("Failed to bind Rect Index Buffer!");

        command_buffer
            .cmd_bind_vertex_buffers(0, vec![(self.vertex_buffer.clone(), 0)])
            .expect("Failed to bind vertex buffer");

        //Now cycle through each primitive call
        for p in call{
            let mut x_add = 0.0;        //Check the character that should be drawn
            let (text, size) = if let Primitive::Text { content, size } = &p.prim {
                (content, size)
            } else {
                println!("widkan: Warning: Attempted to draw a none text with the text primitive!");
                return;
            };

            //Setup the scissors for this call
            command_buffer
                .cmd_set_scissors(vec![p.get_scissors().to_scissors()])
                .expect("Failed to set scissors!");
            
            //Now draw each character
            for c in text.chars() {
                //Check if we have that character, then calculate the drawing area and build the push const accordingly.
                let (draw_area, character_mm) =
                    if let Some(glyph) = self.character_store.characters.get(&c) {
                        let ret = (
                            Area {
                                pos: (
                                    p.area.pos.0 + x_add,
                                    p.area.pos.1 + glyph.offset_down(*size) as f64,
                                ),
                                extent: {
                                    let wh = glyph.width_height(*size);
                                    (wh.0 as f64, wh.1 as f64)
                                },
                            },
                            glyph.min_max,
                        );

                        //Add the width to x_add we have to add for the next character
                        x_add += glyph.offset_to_next(*size) as f64;
                        ret
                    } else {
                        //Check if we got an space. If so, just write a space and continue
                        if c == ' ' {
                            x_add += self.white_space_extent(*size) as f64;
                            continue;
                        }
                        println!(
                            "widkan: Error: Could not get character {} from character pool.",
                            c
                        );
                        //Draw an error symbol
                        (
                            Area {
                                pos: (0.0, 0.0),
                                extent: (0.0, 0.0),
                            },
                            [0.0; 4],
                        )
                    };
                //Build the
                let pc_vert = PushConstant::new(
                    TextPush {
                        transform: draw_area.to_transform(renderer.screen_area()),
                        character_mm,
                        color: p.draw_type.to_color(renderer),
                        level: p.get_level(),
                        pad1: [0.0;3]
                    },
                    vk::ShaderStageFlags::VERTEX | vk::ShaderStageFlags::FRAGMENT,
                );

                //Bind the current correct push const
                command_buffer
                    .cmd_push_constants(self.pipeline.layout(), &pc_vert)
                    .expect("Failed to push triangles constants");

                command_buffer
                    .cmd_draw_indexed(6, 1, 0, 0, 1)
                    .expect("Failed to draw rect!");
            }
        }
    }
}

///The Push Constant content for the texts pipeline
#[repr(C)]
pub struct TextPush {
    pub transform: [[f32; 4]; 4],
    pub character_mm: [f32; 4],
    pub color: [f32;4],
    pub level: f32,
    pub pad1: [f32;3]
}
