/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use crate::widget::*;
use std::path::*;

use msw::winit;
use winit::event;
use winit::event::*;
use winit::dpi::*;
///Some soft wrapper around winits Events which mostly converts the locations from pixel format to the
/// format of `Area` to make intersection tests easier.
#[derive(Clone, Debug)]
pub enum EventType {
    Awaken,
    Suspend,
    /// When a resize occurs. The event caries the new target area for the widget.
    Resize(Area),
    /// When the window was moved. Since the window location does not applies to the interface toolkit,
    /// this mostly does need to be handled. The location is in pixels on the screen.
    Moved((f64, f64)),
    ///When a closing event was send to the window
    Close,
    ///When the window has been destroyed.
    Destroy,
    ReciveCharacter(char),
    ///When a File has been dropped into the window.
    FileDrop(PathBuf),
    HoverFile(PathBuf),
    HoverCancel,
    Focused(bool),
    KeyInput(KeyboardInput),
    ///When the cursor moved within the window bounds
    CursorMoved {
        ///The position relative to the top left of this window, the position is given in pixels.
        position: (f64, f64),
        ///The delta movement in pixel
        delta: (f64, f64),
        modifiers: ModifiersState,
    },
    /// The Raw physical motion of this mouse device. Should not be used as cursor input, more if you want to control for instance a game camera with it.
    MouseMotion {
        delta: (f64, f64),
    },
    CursorEntered,
    CursorLeft,
    MouseWheel {
        //How much in each axis was scrolled. Positive is away from user/right.
        delta: (f64, f64),
        modifiers: ModifiersState,
    },
    MouseClick {
        state: ElementState,
        button: MouseButton,
        modifiers: ModifiersState,
    },
    //TODO At this point I left out TouchpadPressure events which are only supported on some apple devices as well as axis events.
    ///When the window needs to be redrawn. The general redrawing will be handled by the renderer, however, if you need ton detect
    Refresh,
    Touch(Touch),
    HiDpiFactorChanged(f64),
    ///Some button was touched, must not be a mouse or keyboard button.
    Button {
        id: ButtonId,
        state: ElementState,
    },
}

#[derive(Clone, Debug)]
pub struct Event {
    /// The location at which the cursor was when the event happened. Similar to `Area` it is a location relative to the top left.
    /// The location is given in pixels.
    pub location: (f64, f64),
    pub event: EventType,
}

impl Event{
    pub fn to_winit_event(&self) -> event::Event<()>{
        let id = unsafe{winit::window::WindowId::dummy()};
        let d_id = unsafe{DeviceId::dummy()};
	
        match self.event{
            //EventType::Awaken => event::Event::Awakened,
            EventType::Suspend => event::Event::Suspended,
            EventType::Resize(area) => event::Event::WindowEvent{window_id: id, event: WindowEvent::Resized(winit::dpi::PhysicalSize::new(area.extent.0 as u32, area.extent.1 as u32))},
            EventType::Moved(to) => event::Event::WindowEvent{window_id: id, event: WindowEvent::Resized(winit::dpi::PhysicalSize::new(to.0 as u32, to.1 as u32))},
            EventType::Close => event::Event::WindowEvent{window_id: id, event: WindowEvent::CloseRequested},
            EventType::Destroy => event::Event::WindowEvent{window_id: id, event: WindowEvent::Destroyed},
            EventType::ReciveCharacter(ch) => event::Event::WindowEvent{window_id: id, event: WindowEvent::ReceivedCharacter(ch)},
            EventType::FileDrop(ref path) => event::Event::WindowEvent{window_id: id, event: WindowEvent::DroppedFile(path.clone())},
            EventType::HoverFile(ref path) => event::Event::WindowEvent{window_id: id, event: WindowEvent::HoveredFile(path.clone())},
            EventType::HoverCancel => event::Event::WindowEvent{window_id: id, event: WindowEvent::HoveredFileCancelled},
            EventType::Focused(focused) => event::Event::WindowEvent{window_id: id, event: WindowEvent::Focused(focused)},
            EventType::KeyInput(input) => event::Event::WindowEvent{window_id: id, event: WindowEvent::KeyboardInput{device_id: d_id, input, is_synthetic: false}},
            EventType::CursorMoved{position, delta, modifiers} => event::Event::WindowEvent{
                window_id: id,
                event: event::WindowEvent::CursorMoved{
                    device_id: d_id,
                    position: winit::dpi::PhysicalPosition::new(position.0 as f64, position.1 as f64),
                    modifiers
                }
            },
            EventType::MouseMotion{delta} => event::Event::DeviceEvent{device_id: d_id, event: DeviceEvent::MouseMotion{delta}},
            EventType::CursorEntered => event::Event::WindowEvent{window_id: id, event: WindowEvent::CursorEntered{device_id: d_id}},
            EventType::CursorLeft => event::Event::WindowEvent{window_id: id, event: WindowEvent::CursorLeft{device_id: d_id}},
            EventType::MouseWheel{delta, modifiers} => event::Event::WindowEvent{window_id: id, event: WindowEvent::MouseWheel{
                device_id: d_id,
                delta: event::MouseScrollDelta::LineDelta(delta.0 as f32, delta.1 as f32),
                phase: event::TouchPhase::Moved, modifiers
            }},
            EventType::MouseClick{state, button, modifiers} => event::Event::WindowEvent{window_id: id, event: WindowEvent::MouseInput{
                device_id: d_id,
                state,
                button,
                modifiers
            }},
            //EventType::Refresh => {},// event::Event::WindowEvent{window_id: id, event: WindowEvent::Refresh},
            EventType::Touch(touch) => event::Event::WindowEvent{window_id: id, event: WindowEvent::Touch(touch)},
	    /*
            EventType::HiDpiFactorChanged(new_factor) => event::Event::WindowEvent{window_id: id, event: WindowEvent::ScaleFactorChanged{scale_factor: new_factor, new_inner_size: PhysicalSize::new(0, 0)}},
	     */
            EventType::Button{id, state} => event::Event::DeviceEvent{device_id: d_id, event: DeviceEvent::Button{button: id, state}},
	    _ => {
		
		println!("Unspported event conversion!");
		event::Event::DeviceEvent{device_id: d_id, event: DeviceEvent::MouseMotion{delta: (0.0, 0.0)}}
	    }
        }
    }
}

///Takes a collection of winit-events as well as an entry cursor location and generates a list of
/// `Event`s from them. Returns a list of Events as well as the last cursor location after all events have taken place.
pub fn to_events(
    start_cursor_position: (f64, f64),
    event: &winit::event::Event<()>,
    window: &winit::window::Window,
) -> (Vec<Event>, (f64, f64)) {
    //Tracks the cursor location
    let mut cursor_location = start_cursor_position;
    let mut fin_events: Vec<Event> = Vec::new();

    //tracks the dpi value which is needed to convert the logical sizes given by winit
    //into actual pixels
    let mut dpi = window.scale_factor();

    match event {
        event::Event::WindowEvent { window_id: _, event } => {
            match event {
                WindowEvent::Resized(new_size) => {
                    let physical_size = {
                        let intermediate: (f64, f64) = (*new_size).into();

                        (intermediate.0 * dpi, intermediate.1 * dpi)
                    };

                    fin_events.push(Event {
                        location: cursor_location,
                        event: EventType::Resize(Area {
                            pos: (0.0, 0.0),
                            extent: physical_size,
                        }),
                    })
                }
                WindowEvent::Moved(new_location) => {
                    let physical_loc = {
                        let intermediate: (f64, f64) = (*new_location).into();

                        (intermediate.0 * dpi, intermediate.1 * dpi)
                    };

                    fin_events.push(Event {
                        location: cursor_location,
                        event: EventType::Moved(physical_loc),
                    })
                }
                WindowEvent::CloseRequested => fin_events.push(Event {
                    location: cursor_location,
                    event: EventType::Close,
                }),
                WindowEvent::Destroyed => fin_events.push(Event {
                    location: cursor_location,
                    event: EventType::Destroy,
                }),
                WindowEvent::DroppedFile(path) => fin_events.push(Event {
                    location: cursor_location,
                    event: EventType::FileDrop(path.clone()),
                }),
                WindowEvent::HoveredFile(path) => fin_events.push(Event {
                    location: cursor_location,
                    event: EventType::HoverFile(path.clone()),
                }),
                WindowEvent::HoveredFileCancelled => fin_events.push(Event {
                    location: cursor_location,
                    event: EventType::HoverCancel,
                }),
                WindowEvent::ReceivedCharacter(ch) => fin_events.push(Event {
                    location: cursor_location,
                    event: EventType::ReciveCharacter(*ch),
                }),
                WindowEvent::Focused(b) => fin_events.push(Event {
                    location: cursor_location,
                    event: EventType::Focused(*b),
                }),
                WindowEvent::KeyboardInput { device_id: _, input, is_synthetic} => fin_events.push(Event {
                    location: cursor_location,
                    event: EventType::KeyInput(*input),
                }),
                WindowEvent::CursorMoved {
                    device_id: _,
                    position,
                    modifiers,
                } => {
                    let physical_loc = {
                        let intermediate: (f64, f64) = (*position).into();

                        (intermediate.0 * dpi, intermediate.1 * dpi)
                    };

                    let delta = {
                        (
                            physical_loc.0 - cursor_location.0,
                            physical_loc.1 - cursor_location.1,
                        )
                    };

                    //Update events, then set the "current mouse position"
                    fin_events.push(Event {
                        location: cursor_location,
                        event: EventType::CursorMoved {
                            position: physical_loc,
                            delta,
                            modifiers: *modifiers,
                        },
                    });
                    cursor_location = physical_loc;
                }
                WindowEvent::CursorEntered { device_id: _ } => fin_events.push(Event {
                    location: cursor_location,
                    event: EventType::CursorEntered,
                }),
                WindowEvent::CursorLeft { device_id: _ } => fin_events.push(Event {
                    location: cursor_location,
                    event: EventType::CursorLeft,
                }),
                WindowEvent::MouseWheel {
                    device_id: _,
                    delta,
                    phase: _,
                    modifiers,
                } => fin_events.push(Event {
                    location: cursor_location,
                    event: EventType::MouseWheel {
                        delta: match delta {
                            MouseScrollDelta::LineDelta(x, y) => (*x as f64, *y as f64),
                            MouseScrollDelta::PixelDelta(pos) => (*pos).into(),
                        },
                        modifiers: *modifiers,
                    },
                }),
                WindowEvent::MouseInput {
                    device_id: _,
                    state,
                    button,
                    modifiers,
                } => fin_events.push(Event {
                    location: cursor_location,
                    event: EventType::MouseClick {
                        state: *state,
                        button: *button,
                        modifiers: *modifiers,
                    },
                }),
		/*
                WindowEvent::Refresh => fin_events.push(Event {
                location: cursor_location,
                event: EventType::Refresh,
            }),
		 */
                WindowEvent::Touch(t) => fin_events.push(Event {
                    location: cursor_location,
                    event: EventType::Touch(*t),
                }),
                WindowEvent::ScaleFactorChanged{scale_factor, new_inner_size} => {
                    //Update the cursor location, dpi factor and issue a resize event since we have to change the physical size
                    let logical_cursor_pos = (cursor_location.0 / dpi, cursor_location.1 / dpi);
                    let physical_curosr_pos = (
                        logical_cursor_pos.0 * *scale_factor as f64,
                        logical_cursor_pos.1 * *scale_factor as f64,
                    );

                    cursor_location = physical_curosr_pos;

                    fin_events.push(Event {
                        location: cursor_location,
                        event: EventType::HiDpiFactorChanged(*scale_factor),
                    });

                    let new_size: (f64, f64) = (new_inner_size.width as f64, new_inner_size.height as f64);
                    fin_events.push(Event {
                        location: cursor_location,
                        event: EventType::Resize(Area {
                            pos: (0.0, 0.0),
                            extent: (new_size.0 * scale_factor, new_size.1 * scale_factor),
                        }),
                    });

                    //Finally update dpi
                    dpi = *scale_factor;
                }
                _ => {} //println!("widkan: Warning, unhandled input"),
            }
        }
        event::Event::DeviceEvent { device_id: _, event } => {
            match event{
                DeviceEvent::MouseMotion{delta} => fin_events.push(Event{
                    location: cursor_location,
                    event: EventType::MouseMotion{delta: *delta}
                }),
                _ => {} //println!("widkan: Device Events are currently not handled"),
            }
        }
	/*
        event::Event::Awakened => fin_events.push(Event {
        location: cursor_location,
        event: EventType::Awaken,
    }),
	 */
        event::Event::Suspended => fin_events.push(Event {
            location: cursor_location,
            event: EventType::Suspend,
        }),
	_ => {}
    }
    (fin_events, cursor_location)
}

pub fn key_code_to_char(code: VirtualKeyCode, is_capital: bool) -> Option<char> {
    match code {
        VirtualKeyCode::A => {
            if is_capital {
                Some('A')
            } else {
                Some('a')
            }
        }
        _ => None,
    }
}
