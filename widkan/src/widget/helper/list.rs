/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */


use crate::widget::*;
use std::sync::{Arc, RwLock};


///A trait that mus be implemented by each entry in this list.
pub trait ListEntry{
    ///Should return the height that should be available for this entry.
    fn get_height(&self) -> f64;
}

///Is an special helper widget which does not obey the given new_area's y extent, but calculates it form its ListEntrys.
/// This is for instance placed into a ListView
pub struct List{
    area: RwLock<Area>,
    //Saves the child and its height
    children: RwLock<Vec<(f64, Arc<dyn Widget + Send + Sync>)>>,
    current_height: RwLock<f64>,
}

impl List{
    pub fn new() -> Arc<Self>{
        Arc::new(
            List{
                area: RwLock::new(Area::empty()),
                children: RwLock::new(Vec::new()),
                current_height: RwLock::new(0.0),
            }
        )
    }

    pub fn push<T: Send + Sync +'static>(&self, new: Arc<T>)
    where T: ListEntry,
          T: Widget{
        *self.current_height.write().expect("Could not override height") += new.get_height();
        self.children.write().expect("Could not push new child").push((new.get_height(), new));
    }

    pub fn get_current_height(&self) -> f64{
        *self.current_height.read().expect("Could not read current height!")
    }
}


impl Widget for List{
    
    fn render(&self, renderer: &mut Renderer, level: u32) -> Vec<PrimitiveCall>{
        //TODO maybe render some outlines?
        let mut child_calls = Vec::new();
        for (_h, c) in self.children.read().expect("Could not update children").iter(){
            child_calls.append(&mut c.render(renderer, level));
        }
        child_calls
    }
    
    fn update(&self, new_area: Area, events: &Vec<Event>, renderer: &Renderer) -> UpdateRetState{
        let mut child_pos = 0.0;
        let updt_state = UpdateRetState::default();
        for (h, c) in self.children.read().expect("Could not update children").iter(){
            updt_state.merge(c.update(
                Area{
                    pos: (
                        new_area.pos.0,
                        new_area.pos.1 + child_pos
                    ),
                    extent: (
                        new_area.extent.0,
                        *h
                    )
                },
                events,
                renderer
            ));

            child_pos += h;
        }

        updt_state
    }
}
