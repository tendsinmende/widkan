/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use crate::widget::helper::*;
use crate::widget::*;
use crate::widget::layout::*;

use crate::config::Config;
use crate::rendering::Renderer;
use crate::rendering::primitive::*;

use std::sync::{Arc,RwLock};

use crate::msw::winit::event::*;
use crate::widget::events::Event;

pub enum ButtonContent{
    ///The button only contains a centered text element
    Text(String),
    /// The button as a centered text with an icon next to it on the left
    IconTextHorizontal((Ico, String)),
    /// The button has a centered text with an icon on top of it
    IconTextVertical((Ico, String)),
    /// The button has only a icon
    Icon(Ico)
}

pub enum Action<T>{
    StaticFn(Arc<dyn Fn() + Send + Sync>),
    Captured((Arc<dyn Fn(&T) + Send + Sync>, T))
}

impl<T> Action<T>{
    pub fn static_action<F: Fn() + Sync + Send + 'static>(f: F) -> Self{
        Action::StaticFn(Arc::new(f))

    }

    pub fn captured_action<F: Fn(&T) + Send + Sync + 'static>(f: F, data: T) -> Self{
        Action::Captured((Arc::new(f), data))
    }

    pub fn execute(&self){
        match self{
            Action::StaticFn(f) => f(),
            Action::Captured((f, d)) => f(d)
        }
    }
}

pub struct Button<T>
{
    area: RwLock<Area>,
    display: Arc<dyn Widget + Send + Sync>,
    action: Action<T>,
    ///Tracks if the mouse if currently over the button
    mouse_on_button: RwLock<bool>,
    ///Tracks if the button was clicked on the last update
    was_clicked: RwLock<bool>,
    ///Tracks if the button is currently being held down.
    is_clicked: RwLock<bool>,
    default_height: f64,
}

impl<T> Button<T>{
    pub fn new(
        config: &Config,
        content: ButtonContent,
        action: Action<T>,
    ) -> Arc<Self>{

        //TODO depending  on the content, setup a tree
        let (display_tree, height): (Arc<dyn Widget + Send + Sync>, f64) = match content{
            ButtonContent::Text(st) => {
                (TextBox::new(
                    config,
                    st,
                    config.text_size,
                    Alignment::Center,
                    false,
                    false,
                    false,
                ), config.default_button_extent.1)
            },
            ButtonContent::IconTextHorizontal((ico, st)) => {
                (layout::LayoutBox::new(
                    config,
                    Some(
                        helper::Wicon::new(ico)
                    ),
                    Some(
                        TextBox::new(
                            config,
                            st,
                            config.text_size,
                            Alignment::Center,
                            false,
                            false,
                            false,
                        )
                    ),
                    SplitType::HorizontalFixed(Offset::PixelLeft(config.default_button_icon_size as f64)),
                ), config.default_button_extent.1)
            },
            ButtonContent::IconTextVertical((ico, st)) => {
                (layout::LayoutBox::new(
                    config,
                    Some(
                        layout::FloatingArea::new(
                            helper::Wicon::new(ico),
                            Area{
                                pos: (0.0,0.0),
                                extent: (config.default_button_icon_size, config.default_button_icon_size)
                            },
                            layout::Positioning::Alignment(Alignment::Center)
                        )
                    ),
                    Some(
                        TextBox::new(
                            config,
                            st,
                            config.text_size,
                            Alignment::Center,
                            false,
                            false,
                            false,
                        )
                    ),
                    SplitType::VerticalFixed(Offset::PixelLeft(config.default_button_icon_size as f64)),
                ), config.text_size as f64 * 2.0 + config.default_button_icon_size) //TODO remove the magic 2 (is needed since we don't know the max height of the text atm)
            },
            ButtonContent::Icon(ico) => {
                (layout::FloatingArea::new(
                    helper::Wicon::new(ico),
                    Area{
                        pos: (0.0,0.0),
                        extent: (config.default_button_icon_size, config.default_button_icon_size)
                    },
                    layout::Positioning::Alignment(Alignment::Center)
                ), config.default_button_icon_size)
            }
        };

        
        Arc::new(Button {
            area: RwLock::new(Area::empty()),
            display: display_tree,
            action: action,
            mouse_on_button: RwLock::new(false),
            was_clicked: RwLock::new(false),
            is_clicked: RwLock::new(false),
            default_height: height
        })
    }

    fn area(&self) -> Area {
        *self.area.read().expect("Failed to read buttons host area")
    }

    ///Returns true if the button was clicked on the last update. Is nice to use the button primitive when it is not
    ///possible to caputure everythin in the closure.
    pub fn was_clicked(&self) -> bool{
        *self.was_clicked.read().expect("Failed to read was clicked boot")
    }

    ///Returns true if the button is currently being hold down.
    pub fn is_down(&self) -> bool{
        *self.mouse_on_button.read().expect("Could not get mouse on button state") &&
            *self.is_clicked.read().expect("Could not get was clicked state")
    }
}

impl<T> Widget for Button<T>{
    fn render(&self, renderer: &mut Renderer, level: u32) -> Vec<PrimitiveCall> {
        //Depending on the selection render a background
        let mut child_calls = self.display.render(renderer, level + 1);

        //Throw away the missplaced rectangle calls
        child_calls = child_calls.into_iter().filter_map(|c|{
            match c.prim{
                Primitive::Rect => None,
                _ => Some(c)
            }
        }).collect();
        
        
        if *self.mouse_on_button.read().expect("Could not read button mouse state"){
            child_calls.push(PrimitiveCall::new(
                Primitive::Rect,
                self.area(),
                level,
                DrawType::Theme(Theme::BackgroundHighlight),
            ));
        }else{
            child_calls.push(PrimitiveCall::new(
                Primitive::Rect,
                self.area(),
                level,
                DrawType::Theme(Theme::Background),
            ));
        }
        
        child_calls
    }

    fn update(&self, new_area: Area, events: &Vec<Event>, renderer: &Renderer) -> UpdateRetState {

        let mut mouse_state = *self.mouse_on_button.read().expect("Could not read current mouse state for button");
        let mut was_clicked = false;
        let mut is_clicked = *self.is_clicked.read().expect("Could not get is_clicked state");
        
        for ev in events.iter() {
            match ev.event {
                EventType::MouseClick {
                    state,
                    button,
                    modifiers: _,
                } => {
                    if self.area().is_in(ev.location) {
                        if button == MouseButton::Left {
                            if state == ElementState::Pressed {
                                self.action.execute();
                                was_clicked = true;
                                is_clicked = true;
                            }else if state == ElementState::Released{
                                is_clicked = false;
                            }
                        }
                    }
                },
                EventType::CursorMoved{position, delta: _, modifiers: _} => {
                    if self.area().is_in(position){
                        mouse_state = true;
                    }else{
                        mouse_state = false;
                    }
                },
                _ => {}
            }
        }
        *self.was_clicked.write().expect("Could not set was clicked bool") = was_clicked;
        *self.is_clicked.write().expect("Could not set is clicked bool") = is_clicked;
        *self.mouse_on_button.write().expect("Could not set mouse state for button") = mouse_state;
        *self.area.write().expect("Failed to update buttons area") = new_area;
        self.display.update(new_area, events, renderer)
    }
}

impl<T> ListEntry for Button<T>{
    fn get_height(&self) -> f64{
        self.default_height
    }
}
