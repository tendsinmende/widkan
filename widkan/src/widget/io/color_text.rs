/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

/**
 * Author: Senvas
 */

use crate::rendering::{*, primitive::*};
use std::sync::{Arc, RwLock};
use crate::widget::*;
use crate::widget::helper::ListEntry;

pub struct ColorText {
    pub text: String,
    pub size: u32,
    pub color: [f32; 4],

    area: RwLock<Area>
}

impl ColorText {
    pub fn new(text: String, size: u32, color: [f32; 4]) -> Arc<Self> {
        Arc::new(
            ColorText {
                text,
                size,
                color,
                area: RwLock::new(Area::empty())
            }
        )
    }
}

impl Widget for ColorText {
    fn render(&self, renderer: &mut Renderer, level: u32) -> Vec<PrimitiveCall> {
        let mut render_calls = Vec::new();

        render_calls.push(PrimitiveCall::new(
            Primitive::Rect,
            *self.area.read().expect("Failed to read area of colored text"),
            level,
            DrawType::Theme(Theme::Background),
        ));
        
        render_calls.push(PrimitiveCall::new(
            Primitive::Text {
                content: self.text.clone(),
                size: self.size
            },
            *self.area.read().unwrap(),
            level+1,
            DrawType::Color(self.color),
        ));

        render_calls
    }

    fn update(&self, new_area: Area, events: &Vec<Event>, renderer: &Renderer) -> UpdateRetState {
        *self.area.write().unwrap() = new_area;
        Default::default()
    }
}

impl ListEntry for ColorText{
    fn get_height(&self) -> f64{
        self.size as f64 * 1.5 //hacky implementation but works
    }
}
