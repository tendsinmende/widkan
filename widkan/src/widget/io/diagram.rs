/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use crate::rendering::primitive::*;
use crate::widget::*;

use std::sync::{Arc, RwLock};

///How many vertices are drawn per pixel.
pub const LINE_QUALITY: f64 = 0.1;
pub const MARKS_PER_PIXEL: f64 = 0.01;

///Can draw a diagram based on the supplied function
pub struct Diagram<T>{
    area: RwLock<Area>,
    func: T,
    color: [f32;4],
    width: f32,
    min: (f64, f64),
    max: (f64, f64)
}

impl<T> Diagram<T>  where T: Fn(f64) -> f64{
    pub fn new(
        function: T,
        color: [f32;3],
        line_width: f32,
        min_x_y: (f64, f64),
        max_x_y: (f64, f64),
    ) -> Arc<Self>{
        Arc::new(
            Diagram{
                area: RwLock::new(Area::empty()),
                func: function,
                color: [color[0], color[1], color[2], 1.0],
                width: line_width,
                min: min_x_y,
                max: max_x_y,
            }
        )
    }

    pub fn get_area(&self) -> Area{
        *self.area.read().expect("Could not read Diagrams area")
    }
}

impl<T> Widget for Diagram<T> where T: Fn(f64) -> f64{
    fn render(&self, _renderer: &mut Renderer, level: u32) -> Vec<PrimitiveCall>{

        //Now generate coordinates for our field
        let current_area = self.get_area();
        let mut points = Vec::new();

        let extent_x = self.max.0 - self.min.0;
        let extent_y = self.max.1 - self.min.1;

        //How often we have to go
        let split_count = current_area.extent.0 * LINE_QUALITY;
        // The length of one split in pixel
        let split_length = current_area.extent.0 / split_count;
        for x in 0..(split_count.ceil() as u64){
            //Change the current split into the from min/max defined coords.
            let x_coord = x as f64 * split_length / current_area.extent.0;
            let y = (self.func)(self.min.0 + (x_coord * extent_x));

            //We have to add the points in the y inversed space of the renderer.
            points.push((x_coord, -(y / extent_y) + 0.5));
        }

        //Render a background, then the diagram
        vec![
            PrimitiveCall::new(
                Primitive::Rect,
                self.get_area(),
                level,
                DrawType::Theme(Theme::Pattern)
            ),
            //The vertical line
            PrimitiveCall::new(
                Primitive::Line{
                    point_set: vec![(0.5,0.0), (0.5,1.0)], //TODO calc function
                    color: [1.0; 4],
                    line_width: 1.0
                },
                self.get_area(),
                level + 1,
                DrawType::Theme(Theme::Pattern), // Will be overriden
            ),
            //The horizontal line
            PrimitiveCall::new(
                Primitive::Line{
                    point_set: vec![(0.0,0.5), (1.0,0.5)], //TODO calc function
                    color: [1.0; 4],
                    line_width: 1.0
                },
                self.get_area(),
                level + 1,
                DrawType::Theme(Theme::Pattern), // Will be overriden
            ),

            //The horizontal line
            PrimitiveCall::new(
                Primitive::Line{
                    point_set: points, //TODO calc function
                    color: self.color,
                    line_width: self.width
                },
                self.get_area(),
                level + 2,
                DrawType::Theme(Theme::Pattern), // Will be overriden
            )

        ]
    }

    fn update(&self, new_area: Area, _events: &Vec<Event>, _renderer: &Renderer) -> UpdateRetState{

        *self.area.write().expect("Could not update diagram's area") = new_area;
        UpdateRetState::default()
    }
}
