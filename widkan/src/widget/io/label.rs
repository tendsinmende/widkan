/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use crate::config::Config;
use crate::rendering::Renderer;
use crate::rendering::primitive::*;
use crate::widget::{events::*, helper::*, Area, UpdateRetState, Widget, Alignment};

use std::sync::{Arc, RwLock};

///Presents a simple line of text
pub struct Label {
    text: Arc<TextBox>,
    area: RwLock<Area>,
    //Holds the default height if not set by the parent widget.
    height: f64,
}

impl Label {
    pub fn new(
        config: &Config,
        text: &str,
        font_size: Option<u32>,
        alignment: Option<Alignment>,
    ) -> Arc<Self> {
        Arc::new(Label {
            text: text_box::TextBox::new(
                config,
                text.to_string(),
                if let Some(s) = font_size {
                    s
                } else {
                    config.text_size
                },
                if let Some(a) = alignment {
                    a
                } else {
                    Alignment::Center
                },
                false,
                true,
                false,
            ),
            area: RwLock::new(Area::empty()),
            height: config.default_single_line_height,
        })
    }

    pub fn set_text(&self, text: String) {
        self.text.set_text(text);
    }

    pub fn get_text(&self) -> String {
        self.text.get_text()
    }

    pub fn get_area(&self) -> Area {
        *self.area.read().expect("Failed to read labels area")
    }
}

impl Widget for Label {
    fn render(&self, renderer: &mut Renderer, level: u32) -> Vec<PrimitiveCall> {
        
        //Then add the text
        let mut child_calls = self.text.render(renderer, level + 2);
        //Render a normal text background
        child_calls.push(PrimitiveCall::new(
            Primitive::Rect,
            self.get_area(),
            level,
            DrawType::Theme(Theme::Background),
        ));

        child_calls
    }

    fn update(&self, new_area: Area, events: &Vec<crate::widget::events::Event>, renderer: &Renderer) -> UpdateRetState {
        //Change the inner area
        *self.area.write().expect("Failed to set labels area") = new_area;

        self.text.update(new_area, events, renderer)
    }
}

impl ListEntry for Label{
    fn get_height(&self) -> f64{
        self.height
    }
}
