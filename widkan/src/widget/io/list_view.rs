/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use crate::widget::*;
use crate::config::Config;
use std::sync::{Arc};

///Can display a list of widgets which implement the `ListEntry` trait.
pub struct ListView{
    ///The original list used
    list: Arc<helper::List>,
    ///The viewport in which the given list will be laid.
    viewport: Arc<layout::Viewport>
}

impl ListView{
    pub fn new(config: &Config, list: Arc<helper::List>) -> Arc<Self>{
        Arc::new(
            ListView{
                list: list.clone(),
                viewport: layout::Viewport::new(
                    config,
                    layout::ViewDimension::Vertical(0.0),
                    list
                )
            }
        )
    }
}

impl Widget for ListView{
    fn render(&self, renderer: &mut Renderer, level: u32) -> Vec<PrimitiveCall>{
        self.viewport.render(renderer, level)
    }
    fn update(&self, new_area: Area, events: &Vec<Event>, renderer: &Renderer) -> UpdateRetState{
        //First get the current list height and update the virtual area of the viewport with that.
        self.viewport.set_view_dimension(layout::ViewDimension::Vertical(self.list.get_current_height()));
        self.viewport.update(new_area, events, renderer)
    }
}
