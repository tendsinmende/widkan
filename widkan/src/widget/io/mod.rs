/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

/// A clickable button which fires an event.
pub mod button;
pub use button::*;

///Can display a single line of text.
pub mod label;
pub use label::*;

/// A simple panel which covers an area
pub mod panel;
pub use panel::*;

/// colored text
pub mod color_text;
pub use color_text::*;

///can display a single line of text which can be edited by the user.
pub mod text_edit;
pub use text_edit::*;

///can display a single line of text which can be edited by the user.
pub mod number_edit;
pub use number_edit::*;

///can display multiple lines of text and places itself into an vertical
/// viewport, if the text gets too big.
pub mod text_view;
pub use text_view::*;

///Displays a `List` within a vertical viewport.
pub mod list_view;
pub use list_view::*;

///Can display an 2D diagram based on a supplied function
pub mod diagram;
pub use diagram::*;


//TODO
// - Slider
// - Progress bar
// - Overlay
// - HeaderBar
