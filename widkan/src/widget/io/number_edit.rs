/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use crate::widget::*;
use crate::widget::io::*;
use crate::widget::helper::*;
use crate::rendering::Renderer;

use crate::config::Config;


use std::string::ToString;
use std::str::FromStr;
use std::sync::{Arc, RwLock};
use std::time::{Duration, Instant};

///A simple text edit that only allows numbers. Also provides and up and down button.
pub struct NumberEdit<N>
where N: NumberField,
      N: ToString,
      N: FromStr,
{
    value: RwLock<N>,
    text_edit: Arc<TextEdit>,
    default_height: f64,
    b_add: Arc<io::Button<()>>,
    b_sub: Arc<io::Button<()>>,
    display_tree: Arc<dyn Widget + Send + Sync>,

    ///Used to make the "Hold down" behavior smoother.
    last_update: RwLock<Instant>,
}

impl<N: Send + Sync + 'static> NumberEdit<N>
where N: NumberField,
      N: ToString,
      N: FromStr,
{
    pub fn new(
        config: &Config,
        default_value: N,
        text_size: Option<u32>,
        alignment: Option<Alignment>,
    ) -> Arc<Self>{
        let str_value = default_value.to_string();

        let text_edit = io::TextEdit::new(
                    config,
                    &str_value,
                    text_size,
                    alignment
        );

        let b_add = io::Button::new(
            config,
            io::ButtonContent::Text("+".to_string()),
            io::Action::static_action(||{})
        );
        let b_sub = io::Button::new(
            config,
            io::ButtonContent::Text("-".to_string()),
            io::Action::static_action(||{})
        );
        
        let tree = layout::LayoutBox::new(
            config,
            Some(layout::LayoutBox::new(
                config,
                Some(b_add.clone()),
                Some(b_sub.clone()),
                layout::SplitType::VerticalFixed(layout::Offset::PercentLeft(0.5)),
            )),
            Some(text_edit.clone()),
            layout::SplitType::HorizontalFixed(layout::Offset::PixelLeft(config.default_single_line_height)),
        );
        
        //TODO Setup a tree with two buttons that refer to a pre build "SELF".
        let f_number_edit = Arc::new(
            NumberEdit{
                value: RwLock::new(default_value),
                text_edit: text_edit.clone(),
                default_height: config.default_single_line_height,
                b_add,
                b_sub,
                display_tree: tree,
                last_update: RwLock::new(Instant::now())
            }
        );
        f_number_edit
    }
    
    fn add_one(&self){
        let mut val = if let Ok(v) = self.get_value(){
            v
        }else{
            self.text_edit.set_text("Error!".to_string());
            return;
        };
        *self.value.write().expect("Could not write value!") = val.add_one();
        self.text_edit.set_text(val.add_one().to_string());
    }

    fn sub_one(&self){
        let mut val = if let Ok(v) = self.get_value(){
            v
        }else{
            self.text_edit.set_text("Error!".to_string());
            return;
        };
        *self.value.write().expect("Could not write value!") = val.sub_one();
        self.text_edit.set_text(val.sub_one().to_string());
    }
    
    pub fn get_value(&self) -> Result<N, <N as FromStr>::Err>{
        let content = self.text_edit.get_text();
        content.parse::<N>()
    }

    pub fn set_value(&self, new_value: N){
        let valstr = new_value.to_string();
        *self.value.write().expect("Failed to write to value!") = new_value;
        self.text_edit.set_text(valstr)
    }

    fn updated(&self){
        *self.last_update.write().expect("Could not update instance") = Instant::now();
    }
}

impl<N: Send + Sync + 'static> Widget for NumberEdit<N>
where N: NumberField,
      N: ToString,
      N: FromStr,{
    
    fn render(&self, renderer: &mut Renderer, level: u32) -> Vec<PrimitiveCall>{
        //TODO add an up and down button that changes the current value

        self.display_tree.render(renderer, level)
    }
    
    fn update(&self, new_area: Area, events: &Vec<Event>, renderer: &Renderer) -> UpdateRetState{

        //Filter out all inputs that are no numbers or points
        let filtered_events: Vec<_> = events.clone().into_iter().filter_map(|ev|{
            match ev.event{
                EventType::MouseClick{state: _, button: _, modifiers: _} => Some(ev),
                EventType::ReciveCharacter(ref c) => {
                    if c.is_ascii_digit() || c.is_control() || *c == '.' || *c == '-'{
                        Some(ev)
                    }else{
                        None
                    }
                },
                _ => Some(ev)
            }
        }).collect();
        let updates = self.display_tree.update(new_area, &filtered_events, renderer);

        if self.b_add.was_clicked(){
            self.add_one();
        }

        if self.b_add.is_down(){
            if self.last_update.read().expect("Could not get instant").elapsed() > Duration::from_secs_f32(0.1){
                self.add_one();
                self.updated();
            }
        }
        
        if self.b_sub.was_clicked(){
            self.sub_one();
        }
        
        if self.b_sub.is_down(){
            if self.last_update.read().expect("Could not get instant").elapsed() > Duration::from_secs_f32(0.1){
                self.sub_one();
                self.updated();
            }
        }

        if !self.b_add.is_down() && !self.b_sub.is_down(){
            self.updated();
        }
        
        updates
    }

}

impl<N> ListEntry for NumberEdit<N>
where N: NumberField,
      N: ToString,
      N: FromStr,
{
    fn get_height(&self) -> f64{
        self.default_height
    }
}


pub trait NumberField{
    fn add_one(&mut self) -> Self;
    fn sub_one(&mut self) -> Self;
}

//At this point I was too lazy to implement a derive macro, so here we go...
//Note: It is only implemented for ixx and fxx at the moment
impl NumberField for i32{
    fn add_one(&mut self) -> Self{
        *self + 1
    }

    fn sub_one(&mut self) -> Self{
        *self - 1
    }
}

impl NumberField for i64{
    fn add_one(&mut self) -> Self{
        *self + 1
    }

    fn sub_one(&mut self) -> Self{
        *self - 1
    }
}

impl NumberField for f32{
    fn add_one(&mut self) -> Self{
        *self + 1.0
    }

    fn sub_one(&mut self) -> Self{
        *self - 1.0
    }
}

impl NumberField for f64{
    fn add_one(&mut self) -> Self{
        *self + 1.0
    }

    fn sub_one(&mut self) -> Self{
        *self - 1.0
    }
}
