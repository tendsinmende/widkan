/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

//! A generic panel. A Panel can be a only a surface of a box, or host some child.

use crate::rendering::primitive::*;
use crate::widget::*;

use std::sync::{Arc, RwLock};

pub struct Panel {
    area: RwLock<Area>,
}

impl Panel {
    pub fn new() -> Arc<Self> {
        Arc::new(Panel {
            area: RwLock::new(Area {
                pos: (0.0, 0.0),
                extent: (0.0, 0.0),
            }),
        })
    }
}

impl Widget for Panel {
    fn render(&self, _renderer: &mut Renderer, level: u32) -> Vec<PrimitiveCall> {
        //Just render this panel
        vec![PrimitiveCall::new(
            Primitive::Rect,
            *self.area.read().expect("Could not read rect area"),
            level,
            DrawType::Theme(Theme::Pattern),
        )]
    }
    fn update(&self, new_area: Area, _events: &Vec<Event>, _renderer: &Renderer) -> UpdateRetState {
        //We only have to update our own area
        *self.area.write().expect("Could not write panel area") = new_area;

        //We currently never change any ret state
        Default::default()
    }
}
