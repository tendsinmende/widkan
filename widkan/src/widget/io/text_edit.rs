/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use crate::config::Config;
use crate::widget::helper::*;
use crate::widget::*;

use crate::rendering::primitive::*;
use std::sync::{Arc, RwLock};


///Allows to edit one single line of text.
pub struct TextEdit{
    area: RwLock<Area>,
    text: Arc<TextBox>,
    border_width: u32,
    ///Holds the default height if not set by the parent.
    height: f64,
}

impl TextEdit{
    pub fn new(
        config: &Config,
        default_text: &str,
        text_size: Option<u32>,
        alignment: Option<Alignment>,
    ) -> Arc<Self>{
        Arc::new(
            TextEdit{
                area: RwLock::new(Area::empty()),
                text: TextBox::new(
                    config,
                    default_text.to_string(),
                    if let Some(s) = text_size{
                        s
                    }else{
                        config.text_size
                    },
                    if let Some(a) = alignment{
                        a
                    }else{
                        Alignment::UpLeft
                    },
                    false,
                    true,
                    true
                ),
                border_width: config.slim_border_width,
                height: config.default_single_line_height,
            }
        )
    }

    //Returns the current text in the edit
    pub fn get_text(&self) -> String{
        self.text.get_text()
    }

    pub fn set_text(&self, text: String){
        self.text.set_text(text);
    }

    pub fn get_area(&self) -> Area{
        *self.area.read().expect("Could not read textbox area")
    }
    
    fn text_box_area(&self) -> Area{
        self.get_area().from_frame(self.border_width)
    } 
}

impl Widget for TextEdit{
    fn render(&self, renderer: &mut Renderer, level: u32) -> Vec<PrimitiveCall> {
        //Render the Frame and the drawing areas background

        //First render the text on this level + 2
        let mut child_calls = self.text.render(renderer, level + 2);
        //In this level render the outline of the text edit

        child_calls.push(PrimitiveCall::new(
            Primitive::Rect,
            self.get_area(),
            level,
            DrawType::Theme(Theme::Background),
        ));
        //On level + 1 render the background
        child_calls.push(PrimitiveCall::new(
            Primitive::Rect,
            self.text_box_area(),
            level+1,
            DrawType::Theme(Theme::BackgroundHighlight),
        ));

        child_calls
    }
    
    fn update(&self, new_area: Area, events: &Vec<Event>, renderer: &Renderer) -> UpdateRetState{
        *self.area.write().expect("Failed to update TextEdit area") = new_area;
        self.text.update(self.text_box_area(), events, renderer)
    }
}

impl ListEntry for TextEdit{
    fn get_height(&self) -> f64{
        self.height
    }
}
