use crate::widget::*;
use crate::config::Config;
use std::sync::{Arc, RwLock};
use crate::rendering::primitive::*;

pub enum SLDirection{
    Vertical,
    Horizontal
}

pub struct StaticList{
    area: RwLock<Area>,
    space: f64,
    ///Tracks the child items and their area within this List
    items: RwLock<Vec<Arc<dyn Widget + Send + Sync>>>,
    direction: SLDirection,
}

impl StaticList{
    pub fn new(
        config: &Config,
        items: Vec<Arc<dyn Widget + Send + Sync>>,
        direction: SLDirection
    ) -> Arc<Self>{


        Arc::new(StaticList{
            area: RwLock::new(Area::empty()),
            space: config.border_width as f64,
            items: RwLock::new(items),
            direction,
        })
    }

    pub fn push_item(&self, item: Arc<dyn Widget + Send + Sync>){
        self.items.write().expect("Failed to add item to static list").push(item);
    }
}

impl Widget for StaticList{
    fn render(&self, renderer: &mut Renderer, level: u32) -> Vec<PrimitiveCall>{
        //Render children with spaces between each other.
        let mut child_calls: Vec<PrimitiveCall> = self.items.read().expect("Failed to read list items")
            .iter()
            .map(|i| i.render(renderer, level + 1))
            .fold(Vec::new(), |mut calls, mut this_calls| {
                calls.append(&mut this_calls);
                calls
            });
        //Add a draw call to render this background
        child_calls.push(
            PrimitiveCall::new(
                Primitive::Rect,
                *self.area.read().expect("Failed to read inner area"),
                level,
                DrawType::Theme(Theme::Pattern)
            )       
        );

        child_calls
    }
    fn update(&self, new_area: Area, events: &Vec<Event>, renderer: &Renderer) -> UpdateRetState{
        *self.area.write().expect("Failed to update area!") = new_area;

        let item_count = self.items.read().expect("Failed to get items").len() as f64;
        let item_extent = match self.direction{
            SLDirection::Horizontal => (new_area.extent.0 - ((item_count+1.0) * self.space)) / item_count,
            SLDirection::Vertical => (new_area.extent.1 - ((item_count+1.0) * self.space)) / item_count,
        };
        
        let mut pos_add = self.space;

        let ret_state = UpdateRetState::default();
        for item in self.items.read().expect("failed to get items").iter(){
            let item_area = match self.direction{
                SLDirection::Horizontal => {
                    let area = Area{
                        pos: (
                            new_area.pos.0 + pos_add,
                            new_area.pos.1
                        ),
                        extent: (
                            item_extent,
                            new_area.extent.1
                        )
                    };

                    pos_add += item_extent + self.space;
                    area
                },
                SLDirection::Vertical => {
                    let area = Area{
                        pos: (
                            new_area.pos.0,
                            new_area.pos.1 + pos_add
                        ),
                        extent: (
                            new_area.extent.0,
                            item_extent
                        )
                    };

                    pos_add += item_extent + self.space;
                    area
                }
            };

            ret_state.merge(item.update(item_area, events, renderer));
        }

        ret_state
    }
}
