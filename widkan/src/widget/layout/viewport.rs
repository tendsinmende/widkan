/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use crate::config::Config;
use crate::rendering::primitive::*;
use crate::widget::{*};
use std::sync::{Arc, RwLock};
use msw::winit;

///Sets how big the virtual dimension of the viewport is in pixel.
#[derive(Clone, Copy, Debug)]
pub enum ViewDimension{
    Vertical(f64),
    Horizontal(f64),
}

impl ViewDimension{
    pub fn size(&self) -> f64{
        match self{
            ViewDimension::Vertical(a) => *a,
            ViewDimension::Horizontal(a) => *a,
        }
    }
}

pub struct Viewport{
    area: RwLock<Area>,
    view_dimensions: RwLock<ViewDimension>,

    child: Arc<dyn Widget + Send + Sync>,

    scroll_speed: f64,
    scroll_extent: u32,
    slim_border_extent: u32,
    ///Holds the percentage at which the current viewport is located
    viewport_location: RwLock<f64>,

    //Tracks the selection status of the mouse
    is_selected: RwLock<bool>,
}

impl Viewport{
    pub fn new(config: &Config, view_dimension: ViewDimension, child: Arc<dyn Widget + Send + Sync>) -> Arc<Self>{
        Arc::new(
            Viewport{
                area: RwLock::new(Area::empty()),
                view_dimensions: RwLock::new(view_dimension),
                child,
                scroll_speed: config.scroll_speed,
                scroll_extent: config.scroll_bar_width,
                slim_border_extent: config.slim_border_width,
                viewport_location: RwLock::new(0.0),
                is_selected: RwLock::new(false),
            }
        )
    }

    pub fn set_view_dimension(&self, new_dimensions: ViewDimension){
        *self.view_dimensions.write().expect("Could not set view dimensions!") = new_dimensions;
    }

    pub fn get_view_dimension(&self) -> ViewDimension{
        *self.view_dimensions.read().expect("Could not get view dimensions!")
    }

    pub fn get_area(&self) -> Area{
        *self.area.read().expect("Could not get view host area")
    }

    fn get_percentage(&self) -> f64{
        *self.viewport_location.read().expect("Could not read scrollbar percentage")
    }

    fn get_scroll_bar_area(&self) -> Area{
        let sarea = self.get_area();
        let percentage = self.get_percentage();
        match self.get_view_dimension(){
            ViewDimension::Horizontal(a) => {
                let mut length = if a > sarea.extent.0{
                    (sarea.extent.0 * (sarea.extent.0 / a)).max(self.scroll_extent as f64) 
                }else{
                    //we don't need a scroll bar since the child is smaller then the
                    // Host
                    sarea.extent.0
                };

                //Clamp the length on the current widith to not peek out of our area.
                length = length.min(sarea.extent.0);
                
                //Now find the area we have to cover
                Area{
                    pos: (
                        sarea.pos.0 + ((sarea.extent.0 - length) * percentage),
                        sarea.pos.1 + sarea.extent.1 - (self.scroll_extent + self.slim_border_extent) as f64
                    ),
                    extent: (
                        length,
                        self.scroll_extent as f64
                    )
                }
            },
            ViewDimension::Vertical(a) => {
                let mut length = if a > sarea.extent.1{
                    (sarea.extent.1 * (sarea.extent.1 / a)).max(self.scroll_extent as f64)
                }else{
                    //we don't need a scroll bar since the child is smaller then the
                    // Host
                    sarea.extent.1
                };

                length = length.min(sarea.extent.1);
                //Now find the area we have to cover
                Area{
                    pos: (
                        sarea.pos.0 + sarea.extent.0 - (self.scroll_extent + self.slim_border_extent) as f64,
                        sarea.pos.1 + ((sarea.extent.1 - length) * percentage),
                    ),
                    extent: (
                        self.scroll_extent as f64,
                        length,
                    )
                }
            }
        }
    }
    
    fn get_drawing_area(&self) -> Area{
        let mut sarea = self.get_area();
        match self.get_view_dimension(){
            ViewDimension::Horizontal(_) => {
                //Remove a scrollbar + 2*slim_border at the bottom
                sarea.extent.1 -= (self.scroll_extent + (2 * self.slim_border_extent)) as f64;
                sarea
            },
            ViewDimension::Vertical(_) => {
                sarea.extent.0 -= (self.scroll_extent + 2 * self.slim_border_extent) as f64;
                sarea
            }
        }
    }

    ///Adds the given amount to the viewport location and checks that we don't move outside of the 0-1 range.
    /// Speed of one "amount" is set in the config by scroll_speed
    fn add_scroll_amount(&self, amount: f64){
        let vd = self.get_view_dimension();
        let mut current_view = *self.viewport_location.read().expect("Could not get amount");
        //Clamp within 0.0, 1.0
        current_view = (current_view + (amount * self.scroll_speed * 100.0 / vd.size())).min(1.0).max(0.0);
        *self.viewport_location.write().expect("Could not update amount") = current_view; 
    }
    ///Calculates the percentage moved for `amount` pixels of the slider being moved.
    fn add_move_amount(&self, amount: f64){
        let sarea = self.get_area();
        let max_movement_extent = match self.get_view_dimension(){
            ViewDimension::Horizontal(a) => {
                let length = if a > sarea.extent.0{
                    (sarea.extent.0 * (sarea.extent.0 / a)).max(self.scroll_extent as f64) 
                }else{
                    //we don't need a scroll bar since the child is smaller then the
                    // Host
                    sarea.extent.0
                };
                sarea.extent.0 - length
            },
            ViewDimension::Vertical(a) => {
                let length = if a > sarea.extent.1{
                    (sarea.extent.1 * (sarea.extent.1 / a)).max(self.scroll_extent as f64)
                }else{
                    //we don't need a scroll bar since the child is smaller then the
                    // Host
                    sarea.extent.1
                };
                sarea.extent.1 - length
            }
        };

        //Now calculate the percentage of that we moved and add that to the percentage
        let mut cpers = self.get_percentage();
        cpers = (cpers + (amount / max_movement_extent)).max(0.0).min(1.0);
        *self.viewport_location.write().expect("Could not update after scroll_move") = cpers;
    }

    fn get_viewport_offset(&self) -> (f64, f64){
        //Calculates the offset we need to add to each child widgets location,
        let view_percentage = *self.viewport_location.read().expect("Could not get viewport percentage");
        let current_area = self.get_area();
        //Note: since we have to move upwards if we scrolled down, the values are inverse.
        // also: we remove the current areas extent on the axis to not moved the "lower" edge to the top.
        match self.get_view_dimension(){
            ViewDimension::Horizontal(a) => {
                (- ((a-current_area.extent.0) * view_percentage), 0.0)
            },
            ViewDimension::Vertical(a) => {
                (0.0, -((a-current_area.extent.1) * view_percentage))
            }
        }
    }
}

impl Widget for Viewport{
    fn render(&self, renderer: &mut Renderer, level: u32) -> Vec<PrimitiveCall>{
        //First render the children with the virtual area, then offset each call
        

        let mut child_render_calls = self.child.render(renderer, level + 1);
        
        //Now offset each child by the offset we found
        let drawing_area = self.get_drawing_area(); //TODO actually cut away the bar
        for child in child_render_calls.iter_mut(){
            child.set_scissors_area(drawing_area);
        }
        
        //Paint scrollbar background/fallback if the childs renders
        child_render_calls.push(
            PrimitiveCall::new(
                Primitive::Rect,
                self.get_area(),
                level,
                DrawType::Theme(Theme::Pattern)
            )
        );
        //Render the scroll bar
        child_render_calls.push(
            PrimitiveCall::new(
                Primitive::Rect,
                self.get_scroll_bar_area(),
                level + 1,
                DrawType::Theme(Theme::BackgroundHighlight)
            )
        );

        child_render_calls
        
    }
    fn update(&self, new_area: Area, events: &Vec<Event>, renderer: &Renderer) -> UpdateRetState{
        //Check if there was an scroll event. If so, translate to an percentage change
        let mut is_selected = *self.is_selected.read().expect("Could not read scroll bar mouse status!");
        //Update the inner area first to make it easy to get the current bar location
        *self.area.write().expect("Could not update area") = new_area;

        //Since we don't want to register events that are "below" other widgets, we filter our events that are not in our area.
        let mut child_events = Vec::new();

        for ev in events{

            if new_area.is_in(ev.location){
                child_events.push(ev.clone());
            }
            
            match ev.event{
                EventType::MouseWheel{
                    delta,
                    modifiers: _,
                } => {
                    if new_area.is_in(ev.location){
                        //TODO actually check if we might have to use the x axis here.
                        self.add_scroll_amount(-delta.1);   
                    }
                },
                EventType::MouseClick{
                    state,
                    button,
                    modifiers: _
                } => {
                    if button == winit::event::MouseButton::Left{
                        if state == winit::event::ElementState::Pressed{
                            if self.get_scroll_bar_area().is_in(ev.location){
                                is_selected = true;
                            }
                        }else{
                            is_selected = false;
                        }
                    }
                },
                EventType::CursorMoved{position: _, delta, modifiers: _} => {
                    if is_selected{
                        match self.get_view_dimension(){
                            ViewDimension::Horizontal(_) =>{
                                self.add_move_amount(delta.0);
                            },
                            ViewDimension::Vertical(_) => {
                                self.add_move_amount(delta.1);
                            }
                        }
                    }
                }
                _ => {}
            }
        }

        //Override selections state
        *self.is_selected.write().expect("Failed to override selection state") = is_selected;
        // Now find the correct virtual area and update the childs with that.
        let carea = self.get_area();
        let view_dim = self.get_view_dimension();
        let current_offset = self.get_viewport_offset();
        let virtual_area = Area{
            pos: (carea.pos.0 + current_offset.0, carea.pos.1 + current_offset.1),
            extent: (
                if let ViewDimension::Horizontal(a) = view_dim{
                    a
                }else{
                    carea.extent.0
                },

                if let ViewDimension::Vertical(a) = view_dim{
                    a
                }else{
                    carea.extent.1
                }
            )
        };
        self.child.update(virtual_area, &child_events, renderer)
    }
}
